# Clippy's Challenge

Clippy's Challenge is a two-dimensional puzzle-like game inspired by the retro classicer [Chip's Challenge](https://en.wikipedia.org/wiki/Chip%27s_Challenge).

It does not aim for **full compatibility**, but instead uses JSON as a data format for saving the levels.

## Implemented tiles

The list of implemented tiles (blocks in the level, background, player etc.) is limited.

Currently, the following tiles are implemented:
* Teleport
* Green button
* Blue button
* Tank
* Pink ball
* Bug
* Shoe
* Key
* Computer chip
* Socket
* Door
* Level exit
* Thin wall
* Force floor
* Fire
* Water
* Ice corner
* Ice element

See also the [tag tiles](https://gitlab.com/clippyschallenge/clippyschallenge/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=tile) for more to-be-implemented tiles.

## Level Editor

To access the level editor, access the page at `/editor`.
You can also click the button on the page.
