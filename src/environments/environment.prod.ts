export const environment = {
  production: true,
  showTileTitle: false,
  useIonThumbnail: false,
  socketIo: 'https://clippyschallenge-server.herokuapp.com:443',
  gameTickDuration: 500
};
