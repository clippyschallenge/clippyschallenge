export function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      if (name !== 'constructor') {
        if (!derivedCtor.prototype[name]) {
          derivedCtor.prototype[name] = baseCtor.prototype[name];
        }
      }
    });
  });
}


// TODO: may try https://stackoverflow.com/a/34591477 for better mixin
// or much better this one: https://nch3v.github.io/2016/02/09/Mixins-with-type-checking-in-Typescript/
