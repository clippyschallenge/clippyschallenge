import {Injectable, Inject} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(@Inject(APP_BASE_HREF) private baseHref: string) {
    console.log('APP_BASE_HREF is', this.baseHref);

    if (!this.baseHref.endsWith('/')) {
      this.baseHref += '/';
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const apiReq = req.clone({ url: `${this.baseHref}${req.url}` });
    return next.handle(apiReq);
  }
}
