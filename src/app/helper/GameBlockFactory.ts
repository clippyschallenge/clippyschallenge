import { Bug } from '../models/tiles/bug';
import { Ice } from '../models/tiles/ice';
import { ThinWall } from '../models/tiles/thin-wall';
import { Socket } from '../models/tiles/socket';
import { ForceFloor } from '../models/tiles/force-floor';
import { Water } from '../models/tiles/water';
import { Shoe } from '../models/tiles/shoe';
import { Door } from '../models/tiles/door';
import { Key } from '../models/tiles/key';
import { PinkBall } from '../models/tiles/pink-ball';
import { Tank } from '../models/tiles/tank';
import { ExitDoor } from '../models/tiles/exitdoor';
import { Fire } from '../models/tiles/fire';
import { ComputerChip } from '../models/tiles/computer-chip';
import { BlueButton } from '../models/tiles/blue-button';
import { ToggleWall } from '../models/tiles/toggle-wall';
import { GreenButton } from '../models/tiles/green-button';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { EmptyPlaceholder } from '../models/tiles/empty-placeholder';
import { MoveableBlock } from '../models/tiles/moveable-block';
import { Wall } from '../models/tiles/wall';
import { Player } from '../models/tiles/player';
import { Tile } from '../models/tiles/abstract/tile';
import { WoodBackground } from '../models/tiles/background/wood-background';
import { Teleporter } from '../models/tiles/teleporter';

export class GameBlockFactory {
  /**
   * Creates a Tile which has the Type of the given tile.
   *
   * @param tile (Object) - the type of Tile that should be created
   *
   * @return Tile with the type of the given tile type
   */
  static createGameBlock<T extends Tile>(tile: Tile, levelHandler: LevelHandlerService): T {
    if (tile == null) {
      return;
    }
    switch (tile['@type']) {
      case Player['@type']:
        return new Player(levelHandler) as any;

      case Wall['@type']:
        return new Wall(levelHandler) as any;

      case MoveableBlock['@type']:
        return new MoveableBlock(levelHandler) as any;

      case EmptyPlaceholder['@type']:
        return new EmptyPlaceholder(levelHandler) as any;

      case WoodBackground['@type']:
        return new WoodBackground(levelHandler) as any;

      case GreenButton['@type']:
        return new GreenButton(levelHandler) as any;

      case ToggleWall['@type']:
        return new ToggleWall(levelHandler) as any;

      case BlueButton['@type']:
        return new BlueButton(levelHandler) as any;

      case ComputerChip['@type']:
        return new ComputerChip(levelHandler) as any;

      case Fire['@type']:
        return new Fire(levelHandler) as any;

      case ExitDoor['@type']:
        return new ExitDoor(levelHandler) as any;

      case Tank['@type']:
        return new Tank(levelHandler) as any;

      case PinkBall['@type']:
        return new PinkBall(levelHandler) as any;

      case Key['@type']:
        return new Key(levelHandler) as any;

      case Door['@type']:
        return new Door(levelHandler) as any;

      case Shoe['@type']:
        return new Shoe(levelHandler) as any;

      case Water['@type']:
        return new Water(levelHandler) as any;

      case ForceFloor['@type']:
        return new ForceFloor(levelHandler) as any;

      case Socket['@type']:
        return new Socket(levelHandler) as any;

      case ThinWall['@type']:
        return new ThinWall(levelHandler) as any;

      case Ice['@type']:
        return new Ice(levelHandler) as any;

      case Bug['@type']:
        return new Bug(levelHandler) as any;

        case Teleporter['@type']:
          return new Teleporter(levelHandler) as any;

      default:
        throw new TypeError(tile['@type'] + ' is currently not supported');
    }
  }
}
