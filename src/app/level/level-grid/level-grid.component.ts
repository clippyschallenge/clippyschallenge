import { Component, OnInit, OnDestroy, Output, HostListener, EventEmitter } from '@angular/core';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { CenterDisplayService } from './center-display.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-level-grid',
  templateUrl: './level-grid.component.html',
  styleUrls: ['./level-grid.component.scss'],
})

export class LevelGridComponent implements OnInit, OnDestroy {
  // @Input() levelGrid: Array<Array<Array<Tile>>>;
  @Output() tileClick = new EventEmitter<Tile>();

  public levelGrid: Array<Array<Array<Tile>>>;

  public gridTopOffset = 0;
  public gridLeftOffset = 0;

  private ngUnsubscribe = new Subject();

  constructor(private levelService: LevelHandlerService,
              private centerDisplayService: CenterDisplayService) { }

  ngOnInit() {
    this.levelGrid = this.levelService.getLevelGridTiles();

    // forward display service events to resize function
    this.levelService.triggerSizeUpdate$.pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe(this.onResize.bind(this));
  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  tileClicked(tile: Tile) {
    this.tileClick.emit(tile);
  }

  @HostListener('window:resize', [])
  onResize() {
    const offsets = this.centerDisplayService.getRecalculatedPosition();

    this.gridTopOffset = offsets.yOffset;
    this.gridLeftOffset = offsets.xOffset;
  }
}
