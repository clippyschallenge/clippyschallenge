import { Injectable } from '@angular/core';

import { LevelHandlerService } from '../../handlers/level.service';

const TOP_OFFSET = 60; // px

@Injectable({
  providedIn: 'root'
})
export class CenterDisplayService {

  constructor(private levelService: LevelHandlerService) { }

  public getRecalculatedPosition() {
    // get virtual sizes
    const [playerPositionY, playerPositionX] = this.levelService.getBlockPosition(this.levelService.player);

    const xMax = this.levelService.getSizeOfDimension('x');
    const yMax = this.levelService.getSizeOfDimension('y');

    // get size of one tile
    const exampleTile = document.querySelector('.tileStack');
    const tileWidth = exampleTile.clientWidth;
    const tileHeight = exampleTile.clientHeight;

    // get real screen HTML elements for real screen size
    const overflowLevelGrid = document.getElementById('overflowLevelGrid');

    // calculate space we have for each tile
    const maxTilesToDisplayX = Math.floor(overflowLevelGrid.clientWidth / tileWidth);
    const maxTilesToDisplayY = Math.floor((overflowLevelGrid.clientHeight - TOP_OFFSET) / tileHeight); // -60px for top bar

    // calculate middle position for player from top left of LevelView;
    const centerTileX = Math.floor(maxTilesToDisplayX / 2);
    const centerTileY = Math.floor(maxTilesToDisplayY / 2);

    // calculate position for LevelView to display in top left in level grid

    /* x axis */
    let topLeftTileX: number;
    // if smaller than screen, we need to fill DisplayedLevel
    if (xMax <= maxTilesToDisplayX) {
      topLeftTileX = Math.floor((maxTilesToDisplayX - xMax) / 2);
    } else {
      // otherwise we need to fit the level grid into the space for the displayed level
      topLeftTileX = -(playerPositionX - centerTileX);

      // if this would need to underflow (right), do not allow underflow
      const overflowRight = (topLeftTileX + xMax - maxTilesToDisplayX);
      if (overflowRight < 0) {
        topLeftTileX = topLeftTileX + Math.abs(overflowRight);
      }

      // if this would cause a gap (left), do not allow underflow
      if (topLeftTileX > 0) {
        topLeftTileX = 0;
      }
    }

    /* y axis */
    let topLeftTileY: number;
    // if smaller than screen, we need to fill DisplayedLevel
    if (yMax <= maxTilesToDisplayY) {
      topLeftTileY = Math.floor((maxTilesToDisplayY - yMax) / 2);
    } else {
      // otherwise we need to fit the level grid into the space for the displayed level
      topLeftTileY = -(playerPositionY - centerTileY);

      // if this would need to underflow (bottom), do not allow underflow
      const overflowBottom = (topLeftTileY + yMax - maxTilesToDisplayY);
      if (overflowBottom < 0) {
        topLeftTileY = topLeftTileY + Math.abs(overflowBottom);
      }

      // if this would cause a gap (top), do not allow underflow
      if (topLeftTileY > 0) {
        topLeftTileY = 0;
      }
    }

    // calculate offset back to pixels
    const xOffset = topLeftTileX * tileWidth;
    const yOffset = topLeftTileY * tileHeight;

    return {xOffset, yOffset: yOffset + TOP_OFFSET};
  }

}
