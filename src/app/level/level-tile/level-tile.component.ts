import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-level-tile',
  templateUrl: './level-tile.component.html',
  styleUrls: ['./level-tile.component.scss'],
})
export class LevelTileComponent implements OnInit {
  @Input() public tile: Tile;
  @Output() click = new EventEmitter<Tile>();

  public showTileTitle: boolean;
  public useIonThumbnail: boolean;

  constructor() { }

  ngOnInit() {
    if (environment.showTileTitle) {
      this.showTileTitle = true;
    }
    if (environment.useIonThumbnail) {
      this.useIonThumbnail = true;
    }
  }

  tileClicked(tile: Tile) {
    this.click.emit(tile);
  }
}
