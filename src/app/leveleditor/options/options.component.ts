import { LevelHandlerService } from 'src/app/handlers/level.service';
import { LevelSaverService } from '../../handlers/level-saver.service';
import { NavController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
// import { FileChooser } from '@ionic-native/file-chooser/ngx';
import JsonLevelList from '../../../assets/levels/levelListSingleplayer.json';
import { MoveDirection } from 'src/app/models/move-direction';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {

  public levelHandler: LevelHandlerService;
  public levelName: string;
  public levelFile: File;

  // TODO: load multiplayer levellist in case of multiplayer
  public levelList: Array<string> = JsonLevelList;

  constructor(
    // private fileChooser: FileChooser,
    public navCtrl: NavController,
    public levelService: LevelSaverService
  ) { }

  ngOnInit(): void {
    // TODO: load levellist
  }
  public changeLevelFile(event): void {
    this.levelFile = event.target.files[0];
  }

  public reorderLevelList(event): void {
    const itemMove = this.levelList.splice(event.detail.from, 1)[0];
    this.levelList.splice(event.detail.to, 0, itemMove);
    event.detail.complete();
  }

  public uploadLevel() {
    // for api see https://stackoverflow.com/a/47938117

    // for android see https://youtu.be/3f0oLzDazS0?t=467
    // this.fileChooser.open().then((uri) => {
    //   alert(uri);
    // });

    const reader = new FileReader();
    reader.readAsText(this.levelFile, 'UTF-8');
    reader.onload = (evt) => {
      const jsonLevel = JSON.parse(reader.result.toString());
      this.levelHandler.deserialize(jsonLevel, false);
    };
  }

  public downloadLevel() {
    this.levelHandler.levelName = this.levelName;

    const jsonLevel = this.levelHandler.serializeLevel();

    const element = document.createElement('a');
    element.setAttribute('href', `data:text/json;charset=utf-8,${encodeURIComponent(jsonLevel)}`);
    element.setAttribute('download', `${this.levelName}.json`);

    const event = new MouseEvent('click');
    element.dispatchEvent(event);
  }

  public addLevel() {
    const name = this.levelName.trim();
    if (name !== '' && !this.levelList.includes(name)) {
      this.levelList.push(name);
    }
  }

  public downloadLevelList() {

    const jsonLevelList = JSON.stringify(this.levelList);

    const element = document.createElement('a');
    element.setAttribute('href', `data:text/json;charset=utf-8,${encodeURIComponent(jsonLevelList)}`);
    element.setAttribute('download', `levelList.json`);

    const event = new MouseEvent('click');
    element.dispatchEvent(event);
  }

  public resizeLevelgrid(direction: 'north' | 'south' | 'east' | 'west', count: number) {
    switch (direction) {
      case 'north':
        this.levelHandler.resizeLevelGrid(MoveDirection.north, count);
        break;
      case 'south':
        this.levelHandler.resizeLevelGrid(MoveDirection.south, count);
        break;
      case 'east':
        this.levelHandler.resizeLevelGrid(MoveDirection.east, count);
        break;
      case 'west':
        this.levelHandler.resizeLevelGrid(MoveDirection.west, count);
        break;
    }
  }
}
