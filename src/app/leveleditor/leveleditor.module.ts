import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OptionsComponent } from './options/options.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LeveleditorPage } from './leveleditor.page';
import { ItemsSidebarComponent } from './items-sidebar/items-sidebar.component';
import { GameEngineSharedModule } from '../shared/game-engine.shared.module';
import { ApiInterceptor } from '../helper/apiInterceptor';

const routes: Routes = [
  {
    path: '',
    component: LeveleditorPage
  }
];

@NgModule({
  declarations: [
    LeveleditorPage,
    ItemsSidebarComponent,
    OptionsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    GameEngineSharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
  ],
  entryComponents: [OptionsComponent]
})
export class LeveleditorPageModule { }
