import { Teleporter } from '../../models/tiles/teleporter';
import { Bug } from '../../models/tiles/bug';
import { Ice } from '../../models/tiles/ice';
import { ThinWall } from '../../models/tiles/thin-wall';
import { Socket } from '../../models/tiles/socket';
import { ForceFloor } from '../../models/tiles/force-floor';
import { Water } from '../../models/tiles/water';
import { Shoe } from '../../models/tiles/shoe';
import { Door } from '../../models/tiles/door';
import { Key } from '../../models/tiles/key';
import { PinkBall } from '../../models/tiles/pink-ball';
import { Tank } from '../../models/tiles/tank';
import { ExitDoor } from '../../models/tiles/exitdoor';
import { Fire } from '../../models/tiles/fire';
import { BlueButton } from '../../models/tiles/blue-button';
import { ToggleWall } from '../../models/tiles/toggle-wall';
import { GreenButton } from '../../models/tiles/green-button';
import { OptionsComponent } from './../options/options.component';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { Player } from 'src/app/models/tiles/player';
import { MoveableBlock } from 'src/app/models/tiles/moveable-block';
import { Wall } from 'src/app/models/tiles/wall';
import { WoodBackground } from 'src/app/models/tiles/background/wood-background';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { ComputerChip } from 'src/app/models/tiles/computer-chip';

@Component({
  selector: 'app-items-sidebar',
  templateUrl: './items-sidebar.component.html',
  styleUrls: ['./items-sidebar.component.scss'],
})
export class ItemsSidebarComponent implements OnInit {

  public chipsChallenge1Items: Array<Tile> = new Array(0);

  @Output() itemSelect = new EventEmitter<Tile>();

  constructor(
    private levelHandler: LevelHandlerService,
    private modalController: ModalController,
    private http: HttpClient) { }

  ngOnInit() {
    this.chipsChallenge1Items.push(
      new Player(this.levelHandler), // TODO: this seems to circumvent Angulars automatic DI (this seems awkward)
      new MoveableBlock(this.levelHandler),
      new Wall(this.levelHandler),
      new WoodBackground(this.levelHandler),
      new GreenButton(this.levelHandler),
      new ToggleWall(this.levelHandler),
      new BlueButton(this.levelHandler),
      new ComputerChip(this.levelHandler),
      new Fire(this.levelHandler),
      new ExitDoor(this.levelHandler),
      new Tank(this.levelHandler),
      new PinkBall(this.levelHandler),
      new Key(this.levelHandler),
      new Door(this.levelHandler),
      new Shoe(this.levelHandler),
      new Water(this.levelHandler),
      new ForceFloor(this.levelHandler),
      new Socket(this.levelHandler),
      new ThinWall(this.levelHandler),
      new Ice(this.levelHandler),
      new Bug(this.levelHandler),
      new Teleporter(this.levelHandler),
    );

    console.log('Loaded items', this.chipsChallenge1Items);
  }

  itemClicked(item: Tile) {
    console.log('selected item:', item);

    this.itemSelect.emit(item);
  }

  public optionsClicked() {
    this.presentModal();
  }

  private async presentModal() {
    const modal = await this.modalController.create({
      component: OptionsComponent,
      componentProps: {
        levelHandler: this.levelHandler,
        http: this.http
      }
    });
    return await modal.present();
  }
}
