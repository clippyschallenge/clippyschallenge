import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeveleditorPage } from './leveleditor.page';
import { SocketIoModule } from 'ngx-socket-io';

describe('LeveleditorPage', () => {
  let component: LeveleditorPage;
  let fixture: ComponentFixture<LeveleditorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeveleditorPage],
      imports: [
        HttpClientModule,
        SocketIoModule.forRoot( { url: 'http://localhost:4444', options: {} }) // mock NOOP socket service
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeveleditorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
