import { Teleporter } from '../models/tiles/teleporter';
import { ISetting } from './../helper/ISetting';
import { MoveDirection } from './../models/move-direction';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Tile } from '../models/tiles/abstract/tile';

@Component({
  selector: 'app-leveleditor',
  templateUrl: './leveleditor.page.html',
  styleUrls: ['./leveleditor.page.scss']
})
export class LeveleditorPage implements OnInit {
  public selectedItem: Tile;
  public playerCanMove: boolean;
  public settings: Array<ISetting> = new Array<ISetting>(0);
  public previousTeleporterPosition: Array<number>;
  public teleporterConnectedMessage = false;

  /**
   * tile which is edited atm and shows it's tile options
   */
  public clickedTile: Tile;

  constructor(
    private levelHandler: LevelHandlerService
  ) { }

  ngOnInit(): void {
    this.levelHandler.loadLevel('emptyLevel');
  }

  @HostListener('document:keydown', ['$event'])
  // TODO: refactor this. code duplication with level, and add camera movement after the player only sees a part of the level
  keyListenerEvent(event: KeyboardEvent) {
    if (this.levelHandler.player != null) {

      if (this.playerCanMove) {
        if (event.key === 'ArrowUp' || event.key === 'w') {
          this.levelHandler.moveBlock(this.levelHandler.player, MoveDirection.north);
        } else if (event.key === 'ArrowDown' || event.key === 's') {
          this.levelHandler.moveBlock(this.levelHandler.player, MoveDirection.south);
        } else if (event.key === 'ArrowLeft' || event.key === 'a') {
          this.levelHandler.moveBlock(this.levelHandler.player, MoveDirection.west);
        } else if (event.key === 'ArrowRight' || event.key === 'd') {
          this.levelHandler.moveBlock(this.levelHandler.player, MoveDirection.east);
        }
      }
    }
  }

  public itemClicked(item: Tile) {
    this.selectedItem = item;
  }

  public tileClicked(tile: Tile) {
    this.teleporterConnectedMessage = false;

    if (!this.selectedItem) {
      // nothing selected -> show options of this tile
      this.clickedTile = tile;

      if (this.previousTeleporterPosition && this.clickedTile instanceof Teleporter) {
        const teleporter = this.clickedTile as Teleporter;
        const previousTeleporter = this.levelHandler.getZStack(this.previousTeleporterPosition)[teleporter.getStackZCoord()] as Teleporter;
        if (previousTeleporter !== teleporter) {
          teleporter.$positionConnectedTeleporter = this.previousTeleporterPosition;
          previousTeleporter.$positionConnectedTeleporter = this.levelHandler.getBlockPosition(teleporter);
          this.previousTeleporterPosition = null;
          this.teleporterConnectedMessage = true;
        }
      }

      const settingInput = tile.getSettings();

      this.settings = new Array<ISetting>(0);

      settingInput.forEach(setting => {
        this.settings.push({ varname: setting, type: this.getTypeOfSetting(setting)});
      });

      console.log('Read settings of tile:', settingInput, this.settings);

      return;
    }

    const position = this.levelHandler.getBlockPosition(tile);
    this.levelHandler.createNewBlockAtPosition(this.selectedItem, position);

    console.log('replaced old tile', tile, 'with', this.selectedItem);
  }

  public selectOtherTeleporterClick() {
    this.previousTeleporterPosition = this.levelHandler.getBlockPosition(this.clickedTile);
  }

  private getTypeOfSetting(setting: string): string {

    const actualSetting = this.clickedTile[setting];
    const type = typeof actualSetting;
    console.error(setting, actualSetting, type);

    if (type === 'string' && /^#[0-9A-F]{0,6}$/.test(setting)) {
      return 'color';
    }
    if (actualSetting === null) {
      return null;
    }

    return type;
  }

  public isTeleporter(val: any) { return val instanceof Teleporter; }
}
