import { MoveDirection } from '../../move-direction';
import { LevelHandlerService } from '../../../handlers/level.service';
import { Tile } from '../abstract/tile';
import { StackLayer } from '../../stackLayer';

export class WoodBackground extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/background/wood.bmp';
    this.$backgroundColor = '#efc09e';
  }

  public static '@type' = 'WoodBackground';
  public '@type' = 'WoodBackground';
  public name = 'Wood Background';

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection) {
    return true;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.texture;
  }
}
