import { Tile } from 'src/app/models/tiles/abstract/tile';
import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { applyMixins } from 'src/app/helper/mixin';
import { MoveDirection } from '../move-direction';
import { StackLayer } from '../stackLayer';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { KillTile } from './abstract/kill-tile';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { TimeTrigger } from './abstract/time-trigger';

/**
 * @see https://wiki.bitbusters.club/Pink_ball
 */
export class PinkBall extends Tile implements KillTile, TimeTrigger {

  public static '@type' = 'PinkBall';

  public tick: import('@angular/core').EventEmitter<{}> = new EventEmitter<{}>();
  public '@type' = 'PinkBall';
  public name = 'PinkBall';

  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/pinkBall.svg';
  }

  public ngUnsubscribe = new Subject();
  public tickDuration = 2;
  public currentTick = this.tickDuration;

  public interval: TimeTrigger;
  public killTile = true;

  public direction: MoveDirection = MoveDirection.east;

  /**
   * Getter $direction
   */
  @levelEditorSetting()
  public get $direction(): MoveDirection {
    return this.direction;
  }

  /**
   * Setter $direction
   */
  public set $direction(value: MoveDirection) {
    this.direction = value;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.moveable;
  }

  public onTick() {
    const moveSuccessfull = this.levelHandler.moveBlock(this, this.$direction);
    if (!moveSuccessfull) {
      this.$direction = this.mirrorDirection(this.direction);
      this.levelHandler.moveBlock(this, this.$direction);
    }
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);
    if (samePosition) {
      this.levelHandler.killPlayer();
    }
  }
}

// load all functions from the class KillTile and Timer and use them when they are called
applyMixins(PinkBall, [KillTile, TimeTrigger]);
