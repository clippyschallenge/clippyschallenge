import { Tile } from 'src/app/models/tiles/abstract/tile';

export abstract class TimeTrigger extends Tile {

  // after how many ticks this tile should be triggered (1 tick is 500ms)
  public readonly tickDuration: number;

  // helper property to count down ticks which are greater than 1
  public currentTick = this.tickDuration;

  public abstract onTick(): void;
}
