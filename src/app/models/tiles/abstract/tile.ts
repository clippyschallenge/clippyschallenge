import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { StackLayer } from '../../stackLayer';
import { MoveDirection } from '../../move-direction';
import { LevelHandlerService } from '../../../handlers/level.service';
import 'reflect-metadata';

export abstract class Tile {

  public static '@type' = 'abstract';
  public '@type' = 'abstract';

  private imageSource: string;
  private backgroundColor: string;

  public noSize = false;

  constructor(protected levelHandler: LevelHandlerService) {
    this.imageSource = 'assets/default.bmp';
    this.backgroundColor = '#';
  }

  public abstract readonly name: string;

  public deserialize(input: Tile, parameters: Array<object>): Tile {
    this.imageSource = input.imageSource;
    // this.backgroundColor = input.backgroundColor; // as background color is not publicly exposed in level editor, let's not overwrite it
    this.levelHandler = input.levelHandler;
    parameters.forEach(element => {
      if (element instanceof LevelHandlerService) {
        this.levelHandler = element;
      }
    });
    return this;
  }

  public canMoveFromHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return true;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return false;
  }

  public preCheckEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) { }

  public preMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) { }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) { }

  /**
   * Getter $imageSource
   */
  public get $imageSource(): string {
    return this.imageSource;
  }

  /**
   * Setter $imageSource
   */
  @levelEditorSetting()
  public set $imageSource(value: string) {
    this.imageSource = value;
  }

  /**
   * Getter $backgroundColor
   */
  public get $backgroundColor(): string {
    return this.backgroundColor;
  }

  /**
   * Setter $backgroundColor
   */
  public set $backgroundColor(value: string) {
    // throw if not valid color
    if (!/^#[0-9A-F]{6}$/i.test(value)) {
      throw new TypeError(`${value} is not a valid hex color.`);
    }

    this.backgroundColor = value;
  }

  public abstract getStackZCoord(): StackLayer;

  public getSettings() {
    const fields = Array<string>();
    let target = Object.getPrototypeOf(this);
    while (target !== Object.prototype) {
      const childFields = Reflect.getOwnMetadata('levelEditorSetting', target) || [];
      childFields.forEach(element => {
        fields.push(element);
      });
      target = Object.getPrototypeOf(target);
    }
    return fields;
  }

  protected mirrorDirection(direction: MoveDirection): MoveDirection {
    if (direction === MoveDirection.north) {
      return MoveDirection.south;
    } else if (direction === MoveDirection.south) {
      return MoveDirection.north;
    } else if (direction === MoveDirection.east) {
      return MoveDirection.west;
    } else if (direction === MoveDirection.west) {
      return MoveDirection.east;
    }
  }

  protected nextDirection(direction: MoveDirection): MoveDirection {
    if (direction === MoveDirection.east) {
      return MoveDirection.south;
    } else if (direction === MoveDirection.south) {
      return MoveDirection.west;
    } else if (direction === MoveDirection.west) {
      return MoveDirection.north;
    } else if (direction === MoveDirection.north) {
      return MoveDirection.east;
    }
  }

  destroy(): void { }
}
