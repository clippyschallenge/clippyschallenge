import { SwitchColor } from '../../switchColor';
import { LevelHandlerService } from '../../../handlers/level.service';
import { Tile } from './tile';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';

export abstract class ColorItem extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
  }

  public static '@type' = 'ColorItem';
  public '@type' = 'ColorItem';
  public name = 'ColorItem';

  public color: SwitchColor;

  /**
   * Getter $color
   */
  @levelEditorSetting()
  public get $color(): SwitchColor {
    return this.color;
  }

  /**
   * Setter $color
   */
  public set $color(value: SwitchColor) {
    this.color = value;
  }
}
