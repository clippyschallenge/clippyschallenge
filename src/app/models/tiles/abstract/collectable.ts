import { applyMixins } from 'src/app/helper/mixin';
import { SwitchColor } from '../../switchColor';
import { StackLayer } from '../../stackLayer';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { MoveDirection } from '../../move-direction';
import { LevelHandlerService } from '../../../handlers/level.service';
import { ColorItem } from './colorItem';

export abstract class Collectable extends Tile implements ColorItem {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
  }

  public static '@type' = 'Collectable';
  public '@type' = 'Collectable';
  public name = 'Collectable';

  // TODO: find possibility to turn this to protected
  public color: SwitchColor;

  /**
   * Getter $color
   */
  public get $color(): SwitchColor {
    if (this.color) {
      return this.color;
    } else {
      return null;
    }
  }

  /**
   * Setter $color
   */
  public set $color(value: SwitchColor) {
    this.color = value;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return true;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);

    if (samePosition) {
      const addedSuccessfully = this.levelHandler.addToInventory(this);
      if (addedSuccessfully) {
        this.levelHandler.removeBlock(this);
      }
    }
  }
}

applyMixins(Collectable, [ColorItem]);
