import { SwitchColor } from '../../switchColor';
import { LevelHandlerService } from '../../../handlers/level.service';
import { Tile } from './tile';
import { StackLayer } from '../../stackLayer';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { SwitchTrigger } from './switch-trigger';

export abstract class Switchable extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
  }

  public static '@type' = 'Switch';
  public '@type' = 'Switch';
  public name = 'Switch';

  @levelEditorSetting()
  public switchColor: SwitchColor;

  public currentState = false;

  /**
   * Getter $currentState
   */
  @levelEditorSetting()
  public get $currentState(): boolean {
    return this.currentState;
  }

  /**
   * Setter $currentState
   */
  public set $currentState(value: boolean) {
    this.currentState = value;
    if (this.currentState) {
      this.onSwitchOn();
    } else {
      this.onSwitchOff();
    }
  }

  /**
   * useless here
   */
  public getStackZCoord(): StackLayer {
    return null;
  }

  /**
   * switches the tile and calls onSwitchOn and onSwitchOff
   * @param switcherTile switcherBlock that triggered the switch (the activator button not the player or monster)
   */
  public switchBlock(switcherTile: SwitchTrigger) {
    this.currentState = !this.currentState;

    if (this.currentState) {
      this.onSwitchOn(switcherTile);
    } else {
      this.onSwitchOff(switcherTile);
    }
  }

  /**
   * what does this tile when it is switched on by another tile
   * @param switcherTile switcherBlock that triggered the switch (the activator button not the player or monster).
   * It is null when it is switched in the LevelEditor
   */
  protected onSwitchOn(switcherTile: SwitchTrigger = null) { }

  /**
   * what does this tile when it is switched off by another tile
   * @param switcherBlock switcherBlock that triggered the switch (the activator button not the player or monster).
   * It is null when it is switched in the LevelEditor
   */
  protected onSwitchOff(switcherBlock: SwitchTrigger = null) { }

  /**
   * in case the image is switched by the switchColor is has to be done here
   */
  public switchImage() { }
}
