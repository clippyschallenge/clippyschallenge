import { Tile } from 'src/app/models/tiles/abstract/tile';
import { MoveDirection } from '../../move-direction';
import { LevelHandlerService } from '../../../handlers/level.service';
import { Switchable } from './switchable';

export abstract class SwitchTrigger extends Switchable {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
  }

  public static '@type' = 'Switcher';
  public '@type' = 'Switcher';
  public name = 'Switcher';

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    if (this.switchColor) {
      this.levelHandler.SwitchColorGroup(this.switchColor, this);
    }
  }
}
