import { MoveDirection } from '../../move-direction';
import { LevelHandlerService } from '../../../handlers/level.service';
import { Tile } from './tile';

export abstract class KillTile extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = '';
  }

  public static '@type' = 'KillTile';
  public '@type' = 'KillTile';
  public name = 'KillTile';

  // just for mixing checking
  public killTile = true;

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return true;
  }

  // Has to be copied into the other class in case that this class is an interface and a Tile is extended
  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);
    if (samePosition) {
      this.levelHandler.killPlayer();
    }
  }
}
