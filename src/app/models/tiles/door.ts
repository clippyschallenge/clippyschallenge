import { ColorItem } from './abstract/colorItem';
import { Player } from 'src/app/models/tiles/player';
import { MoveDirection } from '../move-direction';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { SwitchColor } from '../switchColor';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';
import { applyMixins } from 'src/app/helper/mixin';
import { Key } from './key';

/**
 * @see https://wiki.bitbusters.club/Keys_and_locks#Locks
 */
export class Door extends Tile implements ColorItem {

  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$color = SwitchColor.red;
  }

  public static '@type' = 'Door';
  public '@type' = 'Door';
  public name = 'Door';

  // TODO: find possibility to change this to protected
  public color: SwitchColor;

  /**
   * Getter $color
   */
  @levelEditorSetting()
  public get $color(): SwitchColor {
    return this.color;
  }

  /**
   * Setter $color
   */
  public set $color(value: SwitchColor) {
    this.color = value;
    this.switchImage();
  }

  public open = false;

  /**
   * Getter $open
   */
  @levelEditorSetting()
  public get $open(): boolean {
    return this.open;
  }

  /**
   * Setter $color
   */
  public set $open(value: boolean) {
    this.open = value;
    this.switchImage();
  }

  public switchImage() {
    if (this.$open) {
      this.$imageSource = `assets/color/door_${SwitchColor[this.color]}_open.svg`;
    } else {
      this.$imageSource = `assets/color/door_${SwitchColor[this.color]}_closed.svg`;
    }
  }


  public getStackZCoord(): StackLayer {
    return StackLayer.block;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    if (this.open) {
      return true;
    }

    if (blockToMove instanceof Player) {
      const inventory = this.levelHandler.getInventory();

      // create a colored key to check if it is in the inventory
      const key = new Key(this.levelHandler);
      key.$color = this.$color;
      const hasSpecificKey = inventory.hasItemInInventory(key, 'colorItem');

      if (!hasSpecificKey) {
        return false;
      }

      // remove door
      this.levelHandler.removeBlock(this);
      return hasSpecificKey;
    }
  }

  public canMoveFromHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return this.$open;
  }
}

applyMixins(Door, [ColorItem]);
