import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';

/**
 * @see https://wiki.bitbusters.club/Wall
 */
export class Wall extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/wall.bmp';
  }

  public static '@type' = 'Wall';
  public '@type' = 'Wall';
  public name = 'Wall';

  public getStackZCoord(): StackLayer {
    return StackLayer.block;
  }
}
