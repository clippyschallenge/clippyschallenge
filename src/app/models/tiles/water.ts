import { MoveableBlock } from 'src/app/models/tiles/moveable-block';
import { MoveDirection } from '../move-direction';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';
import { KillTile } from './abstract/kill-tile';
import { ShoeType } from '../shoeType';

/**
 * @see https://wiki.bitbusters.club/Water
 */
export class Water extends KillTile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/water.svg';
  }

  public static '@type' = 'Water';
  public '@type' = 'Water';
  public name = 'Water';

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {

    // Player
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);
    if (samePosition) {
      // check if a shoe can save the player
      const inventory = this.levelHandler.getInventory();
      if (inventory.hasItemInInventory(ShoeType.water, 'shoeItem')) {
        return;
      }

      this.levelHandler.killPlayer();
      return;
    }

    // Monster
    if (tileStack[StackLayer.block] instanceof KillTile || (tileStack[StackLayer.block] as KillTile).killTile) {
      this.levelHandler.removeBlock(tileStack[StackLayer.moveable]);
      return;
    }

    // Moveable tile
    if (tileStack[StackLayer.block] instanceof MoveableBlock) {
      this.levelHandler.removeBlock(tileStack[StackLayer.moveable]);
      this.levelHandler.removeBlock(this);
      return;
    }
  }

}
