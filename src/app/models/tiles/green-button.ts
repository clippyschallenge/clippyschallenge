import { SwitchColor } from '../switchColor';
import { StackLayer } from '../stackLayer';
import { MoveDirection } from '../move-direction';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { SwitchTrigger } from './abstract/switch-trigger';

/**
 * @see https://wiki.bitbusters.club/Buttons#Green_button
 */
export class GreenButton extends SwitchTrigger {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/greenButton.svg';
  }

  public static '@type' = 'GreenButton';
  public '@type' = 'GreenButton';
  public name = 'Green Button';
  switchColor = SwitchColor.green;

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public canMoveToHere(): boolean {
    return true;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {

    const position = this.levelHandler.getBlockPosition(this);
    const positionOtherBlock = this.levelHandler.getBlockPosition(blockToMove);

    if (position[0] === positionOtherBlock[0] && position[1] === positionOtherBlock[1]) {
      // same position
      this.levelHandler.SwitchColorGroup(this.switchColor, this);
    }
  }
}
