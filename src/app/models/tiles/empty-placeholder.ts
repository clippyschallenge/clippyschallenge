import { applyMixins } from 'src/app/helper/mixin';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { SwitchColor } from '../switchColor';
import { StackLayer } from '../stackLayer';
import { MoveDirection } from '../move-direction';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { ColorItem } from './abstract/colorItem';

/**
 * empty tile used for filling and prevent exceptions when nothing is at the position
 */
export class EmptyPlaceholder extends Tile implements ColorItem {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = '';
  }

  public static '@type' = 'Placeholder';
  public '@type' = 'Placeholder';
  public name = 'Placeholder';

  // TODO: find possibility to turn this to protected
  public color: SwitchColor;

  // tell not to size
  // This is a workaround and needed, because otherwise empty tiles on top of others would overlay lower images on lower layers.
  public noSize = true;

  /**
   * Getter $color
   */
  @levelEditorSetting()
  public get $color(): SwitchColor {
    if (this.color) {
      return this.color;
    } else {
      return null;
    }
  }

  /**
   * Setter $color
   */
  public set $color(value: SwitchColor) {
    this.color = value;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection) {
    return true;
  }

  public getStackZCoord(): StackLayer {
    return null;
  }
}

applyMixins(EmptyPlaceholder, [ColorItem]);
