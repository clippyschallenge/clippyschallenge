import { Bug } from './bug';
import { MoveDirection } from '../move-direction';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { LevelHandlerService } from '../../handlers/level.service';
import { StackLayer } from '../stackLayer';
import { KillTile } from './abstract/kill-tile';
import { ShoeType } from '../shoeType';

/**
 * @see https://wiki.bitbusters.club/Fire
 */
export class Fire extends KillTile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/fire.svg';
  }

  public static '@type' = 'Fire';
  public '@type' = 'Fire';
  public name = 'Fire';

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    if (blockToMove instanceof Bug) {
      return false;
    }
    return true;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);

    if (samePosition) {
      // check if a shoe can save the player
      const inventory = this.levelHandler.getInventory();
      if (inventory.hasItemInInventory(ShoeType.fire, 'shoeItem')) {
        return;
      }

      this.levelHandler.killPlayer();
    }

    // Monster
    if (tileStack[StackLayer.moveable] instanceof KillTile || (tileStack[StackLayer.moveable] as KillTile).killTile) {
      this.levelHandler.removeBlock(tileStack[StackLayer.moveable]);
      return;
    }
  }
}
