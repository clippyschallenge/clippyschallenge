import { ShoeType } from '../shoeType';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { Collectable } from './abstract/collectable';

/**
 * @see https://wiki.bitbusters.club/Item#Items_in_both_versions
 */
export class Shoe extends Collectable {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = `assets/shoe/shoe_${ShoeType[this.shoeType]}.svg`;
  }

  public static '@type' = 'Shoe';
  public '@type' = 'Shoe';
  public name = 'Shoe';

  public shoeType: ShoeType = ShoeType.fire;

  /**
   * Getter $shoeType
   */
  @levelEditorSetting()
  public get $shoeType(): ShoeType {
    return this.shoeType;
  }

  /**
   * Setter $shoeType
   */
  public set $shoeType(value: ShoeType) {
    this.shoeType = value;
    this.switchImage();
  }

  public switchImage() {
    this.$imageSource = `assets/shoe/shoe_${ShoeType[this.shoeType]}.svg`;
  }
}
