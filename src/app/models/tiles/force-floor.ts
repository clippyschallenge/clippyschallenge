import { EmptyPlaceholder } from './empty-placeholder';
import { applyMixins } from 'src/app/helper/mixin';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { MoveDirection } from '../move-direction';
import { Tile } from './abstract/tile';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { StackLayer } from '../stackLayer';
import { EventEmitter } from '@angular/core';
import { TimeTrigger } from './abstract/time-trigger';

// TODO add posibility that tiles can check its position on levelstart -> here it should start move other tiles
/**
 * @see https://wiki.bitbusters.club/Force_floor
 */
export class ForceFloor extends Tile implements TimeTrigger {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/directional/forceFloor_east.svg';
  }

  public static '@type' = 'ForceFloor';

  public '@type' = 'ForceFloor';
  public name = 'ForceFloor';

  public interval: TimeTrigger;
  public tickDuration = 2;
  public currentTick = this.tickDuration;

  public direction: MoveDirection = MoveDirection.east;

  /**
   * Getter $direction
   */
  @levelEditorSetting()
  public get $direction(): MoveDirection {
    return this.direction;
  }

  /**
   * Setter $direction
   */
  public set $direction(value: MoveDirection) {
    this.direction = value;
    this.switchImage();
  }

  public switchImage() {
    this.$imageSource = `assets/directional/forceFloor_${MoveDirection[this.$direction]}.svg`;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection) {
    return true;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const thisPosition = this.levelHandler.getBlockPosition(this);

    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);

    let tile: Tile;

    if (samePosition) {
      tile = this.levelHandler.player;
    } else {
      // for other blocks like a monster
      tile = this.levelHandler.getTileAtPosition(thisPosition, StackLayer.moveable);

      // in case it is an emptyBlock
      if (tile instanceof EmptyPlaceholder) {
        return;
      }
    }
  }


  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public canMoveFromHere(blockToMove: Tile, direction: MoveDirection): boolean {
    // TODO check if its possible to escape via any other site -> then switch === to != and returns
    if (direction === this.mirrorDirection(this.$direction)) {
      // it's not possible to move in the opposite direction as the force floor
      return false;
    }
    return true;
  }

  public onTick() {
    // get the tile that is on this force floor
    const thisPosition = this.levelHandler.getBlockPosition(this);

    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);

    let tile: Tile;

    if (samePosition) {
      tile = this.levelHandler.player;
    } else {
      // for other blocks like a monster
      tile = this.levelHandler.getTileAtPosition(thisPosition, StackLayer.moveable);

      // in case it is an emptyBlock
      if (tile instanceof EmptyPlaceholder) {
        return;
      }
    }

    // move that tile
    this.levelHandler.moveBlock(tile, this.$direction);
  }
}

// load all functions from the classes Timer and use them when they are called
applyMixins(ForceFloor, [TimeTrigger]);
