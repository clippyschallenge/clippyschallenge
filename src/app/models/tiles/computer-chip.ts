import { MoveDirection } from '../move-direction';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { Collectable } from './abstract/collectable';

/**
 * @see https://wiki.bitbusters.club/Computer_chip
 */
export class ComputerChip extends Collectable {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/computerChip.svg';
  }

  public static '@type' = 'ComputerChip';
  public '@type' = 'ComputerChip';
  public name = 'ComputerChip';

  @levelEditorSetting()
  public batteryValue = 1;

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);

    if (samePosition) {
      this.levelHandler.removeBlock(this);
      this.levelHandler.$battery += this.batteryValue;
    }
  }
}
