import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';
import { MoveDirection } from '../move-direction';

/**
 * @see https://wiki.bitbusters.club/Teleport
 */
export class Teleporter extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/teleporter.svg';
  }

  public static '@type' = 'Teleporter';
  public '@type' = 'Teleporter';
  public name = 'Teleporter';

  // private connectedTeleporter: Teleporter;
  public positionConnectedTeleporter: Array<number>;

  /**
   * Getter $positionConnectedTeleporter
   */
  public get $positionConnectedTeleporter() {
    return this.positionConnectedTeleporter;
  }

  /**
   * Setter $positionConnectedTeleporter
   */
  @levelEditorSetting()
  public set $positionConnectedTeleporter(value) {
    this.positionConnectedTeleporter = value;
    // this.connectedTeleporter = (this.levelHandler.getZStack(this.positionConnectedTeleporter)[this.getStackZCoord()] as Teleporter)
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    // true if the other teleporter has that position free
    const teleportedStack = this.levelHandler.getZStack(this.getTeleportedPosition(blockToMove, direction));
    if (teleportedStack && this.levelHandler.executeCanMoveToHere(this, direction, teleportedStack)) {
      return true;
    }
    return false;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    this.levelHandler.moveBlock(blockToMove, direction, this.getTeleportedPosition(blockToMove, direction));
  }

  /**
   * get the position the other teleporter will be leaved
   * @param direction move direction the tile will move out of the teleporter
   */
  private getTeleportedPosition(blockToTeleport: Tile, direction: MoveDirection): Array<number> {
    if (!this.$positionConnectedTeleporter || this.$positionConnectedTeleporter.length === 0) {
      return null;
    }
    const teleportedPosition = Object.assign([], this.$positionConnectedTeleporter);
    switch (direction) {
      case MoveDirection.north:
        teleportedPosition[0] -= 1;
        break;
      case MoveDirection.south:
        teleportedPosition[0] += 1;
        break;
      case MoveDirection.east:
        teleportedPosition[1] += 1;
        break;
      case MoveDirection.west:
        teleportedPosition[1] -= 1;
        break;
    }
    teleportedPosition[2] = blockToTeleport.getStackZCoord();

    return teleportedPosition;
  }
}
