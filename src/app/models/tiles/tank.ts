import { Tile } from 'src/app/models/tiles/abstract/tile';
import { EventEmitter } from '@angular/core';
import { applyMixins } from 'src/app/helper/mixin';
import { MoveDirection } from '../move-direction';
import { SwitchColor } from '../switchColor';
import { StackLayer } from '../stackLayer';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { Switchable } from './abstract/switchable';
import { KillTile } from './abstract/kill-tile';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { SwitchTrigger } from './abstract/switch-trigger';
import { TimeTrigger } from './abstract/time-trigger';

/**
 * @see https://wiki.bitbusters.club/Tank
 */
export class Tank extends Switchable implements KillTile, TimeTrigger {

  public static '@type' = 'Tank';

  public tick: import('@angular/core').EventEmitter<{}> = new EventEmitter<{}>();
  public '@type' = 'Tank';
  public name = 'Tank';
  switchColor = SwitchColor.blue;

  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.switchImage();
  }

  public tickDuration = 2;
  public interval;
  public killTile = true;
  public currentTick = this.tickDuration;

  public direction: MoveDirection = MoveDirection.east;

  /**
   * Getter $direction
   */
  @levelEditorSetting()
  public get $direction(): MoveDirection {
    return this.direction;
  }

  /**
   * Setter $direction
   */
  public set $direction(value: MoveDirection) {
    this.direction = value;
    this.switchImage();
  }

  public switchImage() {
    this.$imageSource = `assets/directional/tank_${MoveDirection[this.$direction]}.svg`;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.moveable;
  }

  protected onSwitchOn(switcherBlock: SwitchTrigger = null) {
    this.onSwitchOff();
  }

  protected onSwitchOff(switcherBlock: SwitchTrigger = null) {
    this.$direction = this.mirrorDirection(this.$direction);
  }

  public onTick() {
    this.levelHandler.moveBlock(this, this.$direction);
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return true;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);
    if (samePosition) {
      this.levelHandler.killPlayer();
    }
  }
}

// load all functions from the class KillTile and Timer and use them when they are called
applyMixins(Tank, [KillTile, TimeTrigger]);
