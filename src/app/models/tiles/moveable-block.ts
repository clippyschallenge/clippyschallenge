import { MoveDirection } from '../move-direction';
import { Tile } from './abstract/tile';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { StackLayer } from '../stackLayer';

// TODO: kill player when tile move to their location
/**
 * @see https://wiki.bitbusters.club/Block
 */
export class MoveableBlock extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/stone.png';
  }

  public static '@type' = 'Dirt';
  public '@type' = 'Dirt';
  public name = 'Movable block';

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection) {
    const currentPosition = this.levelHandler.getBlockPosition(this);
    const currentPositionStack = this.levelHandler.getZStack(currentPosition);

    const lastPosition = this.levelHandler.getLastPosition(currentPosition, direction);
    const lastPositionStack = this.levelHandler.getZStack(lastPosition);

    if (!lastPositionStack.includes(this.levelHandler.player)
      && (this.levelHandler.secondPlayer && !lastPositionStack.includes(this.levelHandler.secondPlayer))) {
      return false;
    }

    const newPosition = this.levelHandler.getNewPosition(currentPosition, direction);

    // in case the new position would not be in the level
    if (newPosition.includes(-1) || currentPosition === newPosition) {
      return false;
    }

    const newPositionStack = this.levelHandler.getZStack(newPosition);

    if (!this.levelHandler.executeCanMoveFromHere(this, direction, currentPositionStack)) {
      return false;
    }

    if (!this.levelHandler.executeCanMoveToHere(this, direction, newPositionStack)) {
      return false;
    }

    return true;
  }

  public preMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    // move this tile before the other is moved

    // TODO: add premoveevent and postmoveevent to the other tile behind this tile

    /// prevent an endless loop
    if (blockToMove !== this) {
      this.levelHandler.moveBlock(this, direction);
    }
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    if (this.levelHandler.blockHasSamePositionAsPlayer(this)) {
      this.levelHandler.killPlayer();
    }
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.moveable;
  }
}
