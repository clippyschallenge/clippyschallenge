import { Tile } from 'src/app/models/tiles/abstract/tile';
import { EventEmitter } from '@angular/core';
import { applyMixins } from 'src/app/helper/mixin';
import { MoveDirection } from '../move-direction';
import { StackLayer } from '../stackLayer';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { KillTile } from './abstract/kill-tile';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { TimeTrigger } from './abstract/time-trigger';
import { Player } from './player';

/**
 * @see https://wiki.bitbusters.club/Bug
 */
export class Bug extends KillTile implements TimeTrigger {

  public static '@type' = 'Bug';

  public tick: import('@angular/core').EventEmitter<{}> = new EventEmitter<{}>();
  public '@type' = 'Bug';
  public name = 'Bug';

  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.switchImage();
  }

  public tickDuration = 2;
  public interval;
  public killTile = true;
  public currentTick = this.tickDuration;

  private myPosition: Array<number>;

  public direction: MoveDirection = MoveDirection.east;

  /**
   * Getter $direction
   */
  @levelEditorSetting()
  public get $direction(): MoveDirection {
    return this.direction;
  }

  /**
   * Setter $direction
   */
  public set $direction(value: MoveDirection) {
    this.direction = value;
    this.switchImage();
  }

  public switchImage() {
    this.$imageSource = `assets/directional/bug_${MoveDirection[this.$direction]}.svg`;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.moveable;
  }

  /**
   * check algorithm for movement priority (where it should move):
   *   2
   * 1 B 3
   *   4
   */
  public onTick() {
    // in case there is no direction setted
    if (this.$direction === null) {
      console.error('this bug has no way to go!');
    }

    if (!this.myPosition) {
      this.myPosition = this.levelHandler.getBlockPosition(this);
    }
    // just for making sure that it will check everything in case it cannot move
    let movedSuccessfully = true;

    if (this.levelHandler.moveBlock(this, this.getMoveDirecton(MoveDirection.west))) {
      this.$direction = this.getMoveDirecton(MoveDirection.west);
    } else if (this.levelHandler.moveBlock(this, this.getMoveDirecton(MoveDirection.north))) {
      this.$direction = this.getMoveDirecton(MoveDirection.north);
    } else if (this.levelHandler.moveBlock(this, this.getMoveDirecton(MoveDirection.east))) {
      this.$direction = this.getMoveDirecton(MoveDirection.east);
    } else if (this.levelHandler.moveBlock(this, this.getMoveDirecton(MoveDirection.south))) {
      this.$direction = this.getMoveDirecton(MoveDirection.south);
    } else {
      movedSuccessfully = false;
    }

    if (movedSuccessfully) {
      this.myPosition = null;
    }
  }

  /**
   * transform a relative MoveDirection into a absolute MoveDirection
   * @example this.direction = south && direction == east -> west
   * @param direction relative moveDirection
   */
  private getMoveDirecton(direction: MoveDirection): MoveDirection {
    switch (this.$direction) {
      case MoveDirection.north:
        return direction;

      case MoveDirection.south:
        return this.mirrorDirection(direction);

      case MoveDirection.east:
        return this.nextDirection(direction);

      case MoveDirection.west:
        return this.mirrorDirection(this.nextDirection(direction));
    }
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    // TODO only if player -> check also other blocks
    return blockToMove instanceof Player;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);
    if (samePosition) {
      this.levelHandler.killPlayer();
    }
  }
}

// load all functions from the class KillTile and Timer and use them when they are called
applyMixins(Bug, [TimeTrigger]);
