import { SwitchColor } from '../switchColor';
import { StackLayer } from '../stackLayer';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { Switchable } from './abstract/switchable';
import { SwitchTrigger } from './abstract/switch-trigger';

/**
 * @see https://wiki.bitbusters.club/Toggle_wall
 */
export class ToggleWall extends Switchable {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    // TODO: find own image of it and exchange the default
    this.$imageSource = 'assets/toggle_wall_closed.svg';
  }

  public static '@type' = 'ToggleWall';
  public '@type' = 'ToggleWall';
  public name = 'ToggleWall';
  switchColor = SwitchColor.green;

  public getStackZCoord(): StackLayer {
    return StackLayer.block;
  }

  public canMoveFromHere(): boolean {
    if (this.currentState) {
      return true;
    } else {
      return false;
    }
  }

  public canMoveToHere(): boolean {
    if (this.currentState) {
      return true;
    } else {
      return false;
    }
  }

  protected onSwitchOn(switcherBlock: SwitchTrigger = null) {
    // TODO: find own image of it and exchange the default
    this.$imageSource = 'assets/toggle_wall_open.svg';
  }

  protected onSwitchOff(switcherBlock: SwitchTrigger = null) {
    // TODO: find own image of it and exchange the default
    this.$imageSource = 'assets/toggle_wall_closed.svg';
  }
}
