import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { applyMixins } from 'src/app/helper/mixin';
import { MoveDirection } from '../move-direction';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { LevelHandlerService } from '../../handlers/level.service';
import { StackLayer } from '../stackLayer';
import { TimeTrigger } from './abstract/time-trigger';

/**
 * @see https://wiki.bitbusters.club/Ice
 */
export class Ice extends Tile implements TimeTrigger {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = `assets/directional/ice_${MoveDirection[this.$direction]}_${MoveDirection[this.direction2]}.svg`;
  }

  public static '@type' = 'Ice';
  public '@type' = 'Ice';
  public name = 'Ice';

  public tickDuration = 1;
  public interval;
  public currentTick = this.tickDuration;

  protected blockMoveDirection: MoveDirection;
  protected blockToMove: Tile;

  // TODO: apparently this will be setted back to default after deserialisation -> the default canot be overwritten
  public direction = null;

  /**
   * Getter $direction
   */
  public get $direction() {
    return this.direction;
  }

  /**
   * Setter $direction
   */
  @levelEditorSetting()
  public set $direction(value) {
    this.direction = value;
    if (this.direction || this.direction === 0) {
      // this.direction2 = this.nextDirection(this.$direction);
    } else {
      // this.direction2 = null;
    }
    this.switchImage();
  }

  public direction2 = this.nextDirection(this.$direction);

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {

    // in case there are no corners
    if (!this.$direction && !this.direction2) {
      return true;
    }

    // in case there are corners
    if (this.$direction === this.mirrorDirection(direction) || this.direction2 === this.mirrorDirection(direction)) {
      return false;
    }
    return true;
  }

  public canMoveFromHere(blockToMove: Tile, direction: MoveDirection): boolean {
    // TODO: check if this is correct

    if (this.blockToMove && direction === this.blockMoveDirection) {
      return true;
    } else if (!this.blockToMove) {
      return true;
    }
    return false;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {

    if (this.blockToMove) {
      return;
    }

    this.blockMoveDirection = direction;
    this.blockToMove = blockToMove;
  }

  public onTick() {
    if (!this.blockToMove) {
      return;
    }
    // check if the tile to move has the same position as this ice tile (prevent too much moving when start from an ice tile)
    const positionStack = this.levelHandler.getZStack(this.levelHandler.getBlockPosition(this.blockToMove));
    if (positionStack.indexOf(this) !== -1) {
      // check in which direction it has to move
      if (!this.$direction && !this.direction2) {
        this.blockMoveDirection = this.blockMoveDirection;
      } else if (this.blockMoveDirection === this.$direction) {
        this.blockMoveDirection = this.mirrorDirection(this.direction2);
      } else if (this.blockMoveDirection === this.direction2) {
        this.blockMoveDirection = this.mirrorDirection(this.$direction);
      }
      this.levelHandler.moveBlock(this.blockToMove, this.blockMoveDirection);
    }
    this.blockToMove = null;
    this.blockMoveDirection = null;
  }

  public switchImage() {
    this.$imageSource = `assets/directional/ice_${MoveDirection[this.$direction]}_${MoveDirection[this.direction2]}.svg`;
  }
}

// load all functions from the class Timer and use them when they are called
applyMixins(Ice, [TimeTrigger]);
