import { MoveDirection } from '../move-direction';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';

/**
 * @see https://wiki.bitbusters.club/Thin_wall
 */
export class ThinWall extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.switchImage();
  }

  public static '@type' = 'ThinWall';
  public '@type' = 'ThinWall';
  public name = 'ThinWall';

  public direction = MoveDirection.north;
  public direction2 = MoveDirection.west;

  /**
   * Getter $direction
   */
  public get $direction() {
    return this.direction;
  }

  /**
   * Setter $direction
   */
  @levelEditorSetting()
  public set $direction(value) {
    this.direction = value;
    this.switchImage();
  }


  /**
   * Getter $direction2
   */
  public get $direction2() {
    return this.direction2;
  }

  /**
   * Setter $direction2
   */
  @levelEditorSetting()
  public set $direction2(value) {
    this.direction2 = value;
    this.switchImage();
  }

  // TODO: all the enemies can not be on the tile layer
  public getStackZCoord(): StackLayer {
    return StackLayer.block;
  }

  /**
   * check if the tile has one given direction
   * @param direction given direction that should be checked
   */
  public hasOneDirection(direction: MoveDirection): boolean {
    return this.$direction === direction || this.$direction2 === direction;
  }

  public canMoveFromHere(blockToMove: Tile, direction: MoveDirection): boolean {
    if (this.$direction === direction || (this.$direction2 && this.$direction2 === direction)) {
      return false;
    }
    return true;
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    if (this.$direction === this.mirrorDirection(direction) || (this.$direction2 && this.$direction2 === this.mirrorDirection(direction))) {
      return false;
    }
    return true;
  }

  public switchImage() {
    this.$imageSource = `assets/directional/thinWall_${MoveDirection[this.$direction]}_${MoveDirection[this.$direction2]}.svg`;
  }
}
