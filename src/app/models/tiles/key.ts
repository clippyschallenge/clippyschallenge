import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';
import { SwitchColor } from '../switchColor';
import { StackLayer } from '../stackLayer';
import { LevelHandlerService } from 'src/app/handlers/level.service';
import { Collectable } from './abstract/collectable';

/**
 * @see https://wiki.bitbusters.club/Keys_and_locks#Keys
 * @see Door Tile
 */
export class Key extends Collectable {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = `assets/color/key_${SwitchColor[this.color]}.svg`;
  }

  public static '@type' = 'Key';
  public '@type' = 'Key';
  public name = 'Key';

  public color: SwitchColor = SwitchColor.red;

  /**
   * Getter $color
   */
  @levelEditorSetting()
  public get $color(): SwitchColor {
    return this.color;
  }

  /**
   * Setter $color
   */
  public set $color(value: SwitchColor) {
    this.color = value;
    this.switchImage();
  }

  public switchImage() {
    this.$imageSource = `assets/color/key_${SwitchColor[this.color]}.svg`;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.floor;
  }

  public canMoveToHere(): boolean {
    return true;
  }
}
