import { Subject } from 'rxjs';
import { MoveDirection } from '../move-direction';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';
import { levelEditorSetting } from 'src/app/helper/levelEditorSettingsProperty';

/**
 * @see https://wiki.bitbusters.club/Socket
 */
export class Socket extends Tile {

  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = `assets/socket_closed.svg`;
  }

  public static '@type' = 'Socket';
  public '@type' = 'Socket';
  public name = 'Socket';

  @levelEditorSetting()
  public chipsRequired = 0;
  private open = false;
  public ngUnsubscribe = new Subject();

  /**
   * Getter $open
   */
  public get $open(): boolean {
    return this.open;
  }

  /**
   * Setter $color
   */
  public set $open(value: boolean) {
    this.open = value;

    if (this.open) {
      this.$imageSource = `assets/socket_open.svg`;
    } else {
      this.$imageSource = `assets/socket_closed.svg`;
    }
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.block;
  }

  public canMoveFromHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return this.$open;
  }

  public canMoveToHere(): boolean {
    if (this.$open) {
      return true;
    } else {
      return false;
    }
  }

  public checkIfNowOpen(chips: number) {
    if (chips >= this.chipsRequired) {
      this.$open = true;
    } else {
      this.$open = false;
    }
  }

  public destroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
