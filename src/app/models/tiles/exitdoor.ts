import { MoveDirection } from '../move-direction';
import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { StackLayer } from '../stackLayer';

/**
 * @see https://wiki.bitbusters.club/Exit
 */
export class ExitDoor extends Tile {
  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/exitDoor.svg';
  }

  public static '@type' = 'ExitDoor';
  public '@type' = 'ExitDoor';
  public name = 'ExitDoor';

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    return true;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.block;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    const samePosition = this.levelHandler.blockHasSamePositionAsPlayer(this);
    if (samePosition) {
      this.levelHandler.loadLevel();
    }
  }
}
