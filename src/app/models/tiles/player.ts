import { LevelHandlerService } from '../../handlers/level.service';
import { Tile } from './abstract/tile';
import { MoveDirection } from '../move-direction';
import { StackLayer } from '../stackLayer';

/**
 * @see https://wiki.bitbusters.club/Chip
 */
export class Player extends Tile {

  constructor(levelHandler: LevelHandlerService) {
    super(levelHandler);
    this.$imageSource = 'assets/person_down.svg';
  }

  public static '@type' = 'Player';
  public '@type' = 'Player';
  public name = 'Clippy';

  public preCheckEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    if (blockToMove === this) {
      switch (direction) {
        case MoveDirection.north:
          this.$imageSource = 'assets/person_up.svg';
          break;
        case MoveDirection.south:
          this.$imageSource = 'assets/person_down.svg';
          break;
        case MoveDirection.east:
          this.$imageSource = 'assets/person_right.svg';
          break;
        case MoveDirection.west:
          this.$imageSource = 'assets/person_left.svg';
          break;
      }
    }
  }

  public canMoveToHere(blockToMove: Tile, direction: MoveDirection): boolean {
    if (blockToMove instanceof Player) {
      return false;
    }
    return true;
  }

  public getStackZCoord(): StackLayer {
    return StackLayer.player;
  }

  public postMoveEvent(blockToMove: Tile, direction: MoveDirection, blockStack: Array<Tile>) {
    this.levelHandler.triggerSizeUpdate$.next();
  }
}
