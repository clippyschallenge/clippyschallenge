export enum StackLayer {
  texture = 0,
  floor = 1,
  block = 2,
  moveable = 3,
  player = 4,
  overlay = 5
}
