export class Room {
  public id: string;
  public playersInRoom: number;
  public name: string;
}
