import { MultiplayerRoomComponent } from './multiplayer-room/multiplayer-room.component';
import { Component, OnInit, Injector } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.page.html',
  styleUrls: ['./main-menu.page.scss']
})
export class MainMenuPage implements OnInit {

  constructor(
    protected injector: Injector
  ) { }

  ngOnInit() {
  }

  public multiplayerClicked() {
    this.presentModal();
  }

  private async presentModal() {
    const modal = await this.modalController.create({
      component: MultiplayerRoomComponent
    });
    return await modal.present();
  }

  // whatever workaround that is: https://stackoverflow.com/a/50331676/5008962
  get modalController(): ModalController {
    return this.injector.get(ModalController);
  }
}
