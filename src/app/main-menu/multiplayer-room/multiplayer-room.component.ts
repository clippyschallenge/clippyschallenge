import { Component, OnInit, OnDestroy } from '@angular/core';
import { MultiplayerService } from 'src/app/handlers/multiplayer.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ModalController, AlertController } from '@ionic/angular';
import { Room } from 'src/app/models/multiplayer/room';
import { ClientConfig } from 'src/app/models/multiplayer/clientconfig';
import { LevelHandlerService } from 'src/app/handlers/level.service';

@Component({
  selector: 'app-multiplayer-room',
  templateUrl: './multiplayer-room.component.html',
  styleUrls: ['./multiplayer-room.component.scss'],
})
export class MultiplayerRoomComponent implements OnInit, OnDestroy {
  rooms: Room[];
  clientConfig: ClientConfig;
  currentRoom: string;
  private rooms$: Subscription;
  private currentRoomSub$: Subscription;
  private startGameSub$: Subscription;
  private clientConfig$: Subscription;

  constructor(
    private multiplayerService: MultiplayerService,
    private modalController: ModalController,
    private router: Router,
    private alertController: AlertController) { }

  ngOnInit() {
    this.clientConfig$ = this.multiplayerService.clientConfig$.subscribe(clientConfig => this.clientConfig = clientConfig);
    this.rooms$ = this.multiplayerService.rooms$.subscribe(rooms => this.rooms = rooms);
    this.currentRoomSub$ = this.multiplayerService.currentRoom$.subscribe(room => this.currentRoom = room.id);
    this.startGameSub$ = this.multiplayerService.startGame$.subscribe(() => this.startGame());

    // manually request the current state from the server, because we need an up-to-date version and the state
    // sent on connect may not be there yet
    this.multiplayerService.pollRoomsStates();
  }

  ngOnDestroy() {
    this.rooms$.unsubscribe();
    this.currentRoomSub$.unsubscribe();
    this.startGameSub$.unsubscribe();
    this.clientConfig$.unsubscribe();
  }

  joinRoom(id: string) {
    this.multiplayerService.joinRoom(id);
  }

  cancel() {
    this.dismissModal();
  }

  private startGame() {
    this.dismissModal();
    this.router.navigate(['home']);
  }

  async newRoom() {
    const roomId: string = await this.presentPrompt('Room ID', 'Please enter room ID', 'Room ID', this.generateRoomId());
    this.multiplayerService.newRoom(roomId);
  }

  private dismissModal() {
    this.modalController.dismiss();
  }

  /**
   * Ask the user for some value to enter.
   */
  private presentPrompt(header: string, message: string, placeholder: string = '', value: string = ''): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const alertPrompt = await this.alertController.create({
        header,
        message,
        inputs: [
          {
            name: 'value',
            placeholder,
            value
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: reject
          },
          {
            text: 'Okay',
            handler: data => { resolve(data.value); }
          }
        ]
      });

      await alertPrompt.present();
    });
  }

  private generateRoomId(): string {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }
}
