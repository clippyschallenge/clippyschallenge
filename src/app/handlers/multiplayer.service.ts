import { MoveDirection } from './../models/move-direction';
import { Room } from '../models/multiplayer/room';
import { ClientConfig } from '../models/multiplayer/clientconfig';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { PlayerChangeEvent } from './playerChangeEvent';
import { Subject } from 'rxjs';
import { RingBuffer } from 'ring-buffer-ts';
import { E2eping } from './e2eping';

@Injectable({
  providedIn: 'root'
})
export class MultiplayerService {
  clientConfig$ = this.socket.fromEvent<ClientConfig>('clientConfig');
  currentRoom$ = this.socket.fromEvent<Room>('joinedRoom');
  playerChange$ = this.socket.fromEvent<PlayerChangeEvent>('playerChange');
  rooms$ = this.socket.fromEvent<Room[]>('rooms');
  errorMessage$ = this.socket.fromEvent<string>('errorMessage');
  startGame$ = this.socket.fromEvent<boolean>('startGame');
  pongLatency$ = this.socket.fromEvent<number>('pong'); // uses internal socketIo pong
  e2eLatency$ = new Subject<number>();

  // internal subscriptions
  private firstPlayerMessage$ = this.socket.fromEvent<boolean>('firstPlayer');
  private tickFinishedOtherClientMessage$ = this.socket.fromEvent<boolean>('tickFinished');

  public currentRoom: string;
  private pingReplyE2e$ = this.socket.fromEvent<number>('e2ePing');
  private pongLatencyE2e$ = this.socket.fromEvent<number>('e2ePong');

  public isMultiplayer = false;
  public isFirstPlayer = false;

  public tickFinished = false;
  public tickFinishedOtherClient = false;
  private pingId = 0;
  private pingBuffer = new RingBuffer<E2eping>(20);
  private e2ePing: number;

  constructor(private socket: Socket) {
    this.currentRoom$.subscribe(room => this.currentRoom = room.id);
    this.firstPlayerMessage$.subscribe(firstPlayer => this.setFirstPlayer(firstPlayer));
    this.tickFinishedOtherClientMessage$.subscribe(tickFinished => this.tickFinishedOtherClient = tickFinished);

    // debugging
    // this.pongLatency$.subscribe((latency) => console.info('Server latency:', latency));

    // e2e ping
    // const e2ePinger = interval(1000);
    this.pongLatencyE2e$.subscribe((pongId) => this.receiveE2ePong(pongId));
    this.pingReplyE2e$.subscribe((pongId) => this.replyToE2ePing(pongId));
    setInterval(() => this.triggerE2ePing(), 1000);
  }

  joinRoom(id: string) {
    this.socket.emit('joinRoom', id);
    this.isMultiplayer = true;
  }

  /**
   * poll the room state, so the server needs to resent the current status
   */
  pollRoomsStates(): void {
    this.socket.emit('getRoomsStates');
  }

  newRoom(roomId: string) {
    this.socket.emit('addRoom', { id: roomId, room: '' });
    this.isMultiplayer = true;
  }

  // send player changes to server and thus to other client
  sendPlayerChange(playerChange: PlayerChangeEvent) {
    if (!this.isMultiplayer) {
      return;
    }

    this.socket.emit('playerChange', { roomId: this.currentRoom, playerChange });
  }

  sendPlayerMove(moveDirection: MoveDirection) {
    switch (moveDirection) {
      case MoveDirection.south:
        this.sendPlayerChange(PlayerChangeEvent.moveDown);
        break;

      case MoveDirection.west:
        this.sendPlayerChange(PlayerChangeEvent.moveLeft);
        break;

      case MoveDirection.north:
        this.sendPlayerChange(PlayerChangeEvent.moveUp);
        break;

      case MoveDirection.east:
        this.sendPlayerChange(PlayerChangeEvent.moveRight);
        break;
    }
  }

  sendTickFinished() {
    if (this.isMultiplayer) {
      this.socket.emit('tickFinished', { roomId: this.currentRoom });
    }
  }

  private setFirstPlayer(firstPlayer: boolean) {
    this.isFirstPlayer = firstPlayer;
    console.log('this is the first player');
  }

  /**
   * Send e2e ping for measurement. The other client is supposed to reply.
   */
  private triggerE2ePing() {
    if (!this.isMultiplayer) {
      return;
    }

    this.pingId++;
    if (this.pingId === 256) {
      this.pingId = 0;
    }

    const e2ePing: E2eping = {
      pingId: this.pingId,
      startTime: Date.now()
    };

    // trigger ping
    this.socket.emit('e2ePing', e2ePing.pingId);

    // save locally
    this.pingBuffer.add(e2ePing);

    // also check whether we reach capacity limits
    if (this.pingBuffer.isFull()) {
      console.error('E2e ping capacity limits reached, other client may not be repliying fast enough.');
    }
  }

  /**
   * Reply to e2e pings by pong'ing them back.
   */
  private replyToE2ePing(pingId) {
    this.socket.emit('e2ePong', pingId);
  }

  /**
   * Evaluate received e2e pongs.
   */
  private receiveE2ePong(pongId) {
    const endTime = Date.now();
    // debugger;

    let e2ePingPacket;
    // iterate buffer to find package
    let bufferIndex;
    for (bufferIndex = 1; bufferIndex < this.pingBuffer.getSize(); bufferIndex++) {
      e2ePingPacket = this.pingBuffer.get(-bufferIndex);
      if (!e2ePingPacket) {
        continue; // if buffer is not full, we need to skip empty entries
      }

      // break loop if packed found
      if (e2ePingPacket.pingId === pongId) {
        break;
      }

      console.warn('e2e pong packet ', e2ePingPacket.pingId, ' is out of order');
    }
    // if still not found, we have an unknown package
    if (e2ePingPacket && (e2ePingPacket.pingId !== pongId)) {
      console.error('e2e pong package with ID', pongId, ' has been received, but was not in ring buffer anymore.');
      return;
    }

    this.e2ePing = endTime - e2ePingPacket.startTime;
    this.e2eLatency$.next(this.e2ePing);

    // delete package
    this.pingBuffer.remove(bufferIndex);
  }
}
