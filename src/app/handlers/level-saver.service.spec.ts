import { TestBed } from '@angular/core/testing';

import { LevelSaverService } from './level-saver.service';
import { StorageMock } from '../test-mocks/ionic-mocks/storage-mock.spec';

import { Storage } from '@ionic/storage';

describe('LevelSaverService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [],
    providers: [
      { provide: Storage, useClass: StorageMock },
    ]
  }));

  it('should be created', () => {
    const service: LevelSaverService = TestBed.get(LevelSaverService);
    expect(service).toBeTruthy();
  });
});
