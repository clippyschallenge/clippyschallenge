import { TestBed } from '@angular/core/testing';

import { TimerService } from './timer.service';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoModule } from 'ngx-socket-io';

describe('TimerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      SocketIoModule.forRoot({ url: 'http://localhost:4444', options: {} }) // mock NOOP socket service
    ]
  }));

  it('should be created', () => {
    const service: TimerService = TestBed.get(TimerService);
    expect(service).toBeTruthy();
  });
});
