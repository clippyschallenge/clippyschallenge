import { MoveDirection } from './../models/move-direction';
import { MultiplayerService } from 'src/app/handlers/multiplayer.service';
import { Injectable } from '@angular/core';
import { TimeTrigger } from '../models/tiles/abstract/time-trigger';
import { environment } from './../../environments/environment';
import { EventBusService } from 'ngx-eventbus';

@Injectable({
  providedIn: 'root'
})
export class TimerService {

  constructor(
    private multiplayerService: MultiplayerService,
    private eventBus: EventBusService) { }

  private interval: ReturnType<typeof setTimeout>;

  public tiles: Array<TimeTrigger> = new Array<TimeTrigger>();
  public nextPlayerMove: MoveDirection;

  public startTimer() {
    if (this.tiles.length >= 1) {
      this.interval = setInterval(() => this.onTick(), environment.gameTickDuration);
    }
  }

  private onTick() {
    if (this.multiplayerService.isMultiplayer
      && (this.multiplayerService.tickFinished && !this.multiplayerService.tickFinishedOtherClient)) {
      // tick is not finished -> do not trigger another one
      return;
    }

    // trigger next tick and reset states
    this.multiplayerService.tickFinished = false;
    this.multiplayerService.tickFinishedOtherClient = false; // may casue soft lock if other client send it too early

    if (this.nextPlayerMove) {
      this.eventBus.triggerEvent('playerMove', { direction: this.nextPlayerMove });
      this.multiplayerService.sendPlayerMove(this.nextPlayerMove);
      this.nextPlayerMove = null;
    }

    this.tiles.forEach(async (tile) => {
      if (tile.currentTick <= 1) {
        tile.currentTick = tile.tickDuration;
        tile.onTick();
      } else {
        tile.currentTick -= 1;
      }
    });

    this.multiplayerService.tickFinished = true;
    this.multiplayerService.sendTickFinished();
  }

  public stopTimer() {
    clearInterval(this.interval);
  }

  public destroy() {
    clearInterval(this.interval);
  }
}
