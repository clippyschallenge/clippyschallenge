import { LevelHandlerService } from 'src/app/handlers/level.service';
import { Tile } from 'src/app/models/tiles/abstract/tile';
import { Injectable, OnInit } from '@angular/core';
import { Switchable } from '../models/tiles/abstract/switchable';
import { SwitchColor } from '../models/switchColor';
import { SwitchTrigger } from '../models/tiles/abstract/switch-trigger';

@Injectable({
  providedIn: 'root'
})
export class TileSwitchServiceService implements OnInit {

  /**
   * dictionary with key of SwitchColor, and a list of SwitchBlocks as value
   * @example use it like an Array: SwitchBlock[SwitchColor][]
   */
  private switchBlocks: { [id: number]: Array<Switchable>; } = {};

  constructor() {
    for (const color in SwitchColor) {
      if (!isNaN(Number(color))) {
        this.switchBlocks[color] = new Array<Switchable>(0);
      }
    }
  }

  ngOnInit(): void { }

  /**
   * load all posible switched and switcher blocks and group them by their switch color
   */
  public loadLevel(levelHandler: LevelHandlerService): void {

    const tiles = levelHandler.getLevelGridTiles();

    tiles.forEach((element, yCoord) => {
      element.forEach((element2, xCoord) => {
        element2.forEach((element3, zCoord) => {
          if (element3 && element3 instanceof Switchable) {
            this.switchBlocks[element3.switchColor].push(element3);
          }
        });
      });
    });
  }

  /**
   * get all gameBlocks by their color
   * @param color color of the group
   */
  public getSwitchGroup(color: SwitchColor): Array<Tile> {
    return this.switchBlocks[color];
  }

  /**
   * switch all Blocks which are in the specific color group
   * @param color color of the Group
   * @param switcherBlock switchBlock that triggered the switch (the activator button not the player or monster)
   */
  public SwitchColorGroup(color: SwitchColor, switcherBlock: SwitchTrigger): void {
    this.switchBlocks[color].forEach((tile) => {
      tile.switchBlock(switcherBlock);
    });
  }
}
