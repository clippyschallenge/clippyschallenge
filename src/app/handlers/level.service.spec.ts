import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { LevelHandlerService } from './level.service';
import { SocketIoModule } from 'ngx-socket-io';

describe('LevelService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      SocketIoModule.forRoot( { url: 'http://localhost:4444', options: {} }) // mock NOOP socket service
    ]
  }));

  it('should be created', () => {
    const service: LevelHandlerService = TestBed.get(LevelHandlerService);
    expect(service).toBeTruthy();
  });
});
