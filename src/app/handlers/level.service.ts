import { TimerService } from './timer.service';
import { Bug } from '../models/tiles/bug';
import { Tank } from '../models/tiles/tank';
import { Shoe } from '../models/tiles/shoe';
import { PinkBall } from '../models/tiles/pink-ball';
import { Key } from '../models/tiles/key';
import { Door } from '../models/tiles/door';
import { ForceFloor } from '../models/tiles/force-floor';
import { Ice } from '../models/tiles/ice';
import { ThinWall } from '../models/tiles/thin-wall';
import { takeUntil } from 'rxjs/operators';
import { Socket } from '../models/tiles/socket';
import { WoodBackground } from 'src/app/models/tiles/background/wood-background';
import { SwitchColor } from './../models/switchColor';
import { TileSwitchServiceService } from './tile-switch-service.service';
import { HttpClient } from '@angular/common/http';
import { GameBlockFactory } from './../helper/GameBlockFactory';
import { Player } from '../models/tiles/player';
import { InventoryHandlerService } from './inventory.service';
import { EmptyPlaceholder } from '../models/tiles/empty-placeholder';
import { MoveDirection } from '../models/move-direction';
import { Tile } from '../models/tiles/abstract/tile';
import { Injectable } from '@angular/core';
import { StackLayer } from '../models/stackLayer';
import JsonLevelListSingleplayer from '../../assets/levels/levelListSingleplayer.json';
import JsonLevelListMultiplayer from '../../assets/levels/levelListMultiplayer.json';
import { SwitchTrigger } from '../models/tiles/abstract/switch-trigger';
import { Collectable } from '../models/tiles/abstract/collectable';
import { TimeTrigger } from '../models/tiles/abstract/time-trigger';
import { Subject } from 'rxjs';
import { Switchable } from '../models/tiles/abstract/switchable';
import { Teleporter } from '../models/tiles/teleporter';
import { PlayerChangeEvent } from './playerChangeEvent';
import { MultiplayerService } from './multiplayer.service';
import { EventBusService } from 'ngx-eventbus';

@Injectable({
  providedIn: 'root'
})

export class LevelHandlerService {
  /**
   * remember: [y][x][z]
   */
  private tiles: Tile[][][] = new Array<Array<Array<Tile>>>(0);
  public player: Player = null;
  public secondPlayer: Player = null;
  protected inventory: InventoryHandlerService = new InventoryHandlerService();
  public levelName: string;

  private xCoordSize = 8;
  private yCoordSize = 8;

  // TODO: load levelList with http synchoniously -> pay attention that the normal level can be loaded outside of the levelEditor
  private levelList: Array<string> = new Array<string>();
  public triggerSizeUpdate$: Subject<any> = new Subject();

  private battery = 0;
  public batteryChanged: Subject<number> = new Subject<number>();

  /**
   * Getter $currentState
   */
  public get $battery(): number {
    return this.battery;
  }

  /**
   * Setter $battery
   */
  public set $battery(value: number) {
    this.battery = value;
    this.batteryChanged.next(this.battery);
  }

  private triggerPlayerMoveEvent: any;

  constructor(
    private http: HttpClient,
    private blockSwitchService: TileSwitchServiceService,
    private multiplayerService: MultiplayerService,
    public timerService: TimerService,
    private eventBus: EventBusService) {
    this.multiplayerService.playerChange$.subscribe(playerChange => this.handlePlayerChangeByEvent(this.secondPlayer, playerChange));

    this.triggerPlayerMoveEvent = this.eventBus.addEventListener({
      name: 'playerMove',
      callback: (directionObject: any) => {
        this.handlePlayerChangeByDirection(this.player, directionObject.direction);
      }
    });
  }

  public deserialize(input: LevelHandlerService, startTimerTiles = true): LevelHandlerService {

    this.levelName = input.levelName;

    while (this.timerService.tiles.length !== 0) {
      this.timerService.tiles.pop();
    }

    while (this.tiles.length !== 0) {
      this.tiles.pop();
    }

    // load size
    this.yCoordSize = input.tiles.length;
    this.xCoordSize = input.tiles[0].length;

    for (let yPosition = 0; yPosition < this.getSizeOfDimension('y'); yPosition++) {

      const column: Tile[][] = new Array<Array<Tile>>();
      for (let xPosition = 0; xPosition < this.getSizeOfDimension('x'); xPosition++) {

        const pane: Tile[] = new Array<Tile>();
        for (let zPosition = 0; zPosition < this.getSizeOfDimension('z'); zPosition++) {

          const tmpObj = input.tiles[yPosition][xPosition][zPosition];
          if (!tmpObj) {
            // it should be an empty tile, which is not saved as one
            pane.push(GameBlockFactory.createGameBlock(new EmptyPlaceholder(this), this));
          } else {
            pane.push(GameBlockFactory.createGameBlock(tmpObj, this));
            pane[zPosition].deserialize(tmpObj, [this]);

            if (this.multiplayerService.isMultiplayer && pane[zPosition] instanceof Player) {
              if (this.multiplayerService.isFirstPlayer) {
                // handle the first player
                if (!(this.player instanceof Player)) {
                  this.player = pane[zPosition];
                } else {
                  this.secondPlayer = pane[zPosition];
                }
              } else {
                // handle the second player
                if (!(this.player instanceof Player) && (this.secondPlayer instanceof Player)) {
                  this.player = pane[zPosition];
                } else {
                  this.secondPlayer = pane[zPosition];
                }
              }
            } else if (pane[zPosition] instanceof Player) {
              this.player = pane[zPosition];
            } else if (pane[zPosition] instanceof Socket) {
              const socket = (pane[zPosition] as Socket);
              socket.chipsRequired = (tmpObj as Socket).chipsRequired;
              this.batteryChanged
                .pipe(takeUntil(socket.ngUnsubscribe))
                .subscribe((value) => socket.checkIfNowOpen(value as number));
            } else if (pane[zPosition] instanceof Ice) {
              (pane[zPosition] as Ice).direction = (tmpObj as Ice).direction;
              (pane[zPosition] as Ice).direction2 = (tmpObj as Ice).direction2;
              (pane[zPosition] as Ice).switchImage();
            } else if (pane[zPosition] instanceof ForceFloor) {
              (pane[zPosition] as ForceFloor).direction = (tmpObj as ForceFloor).direction;
              (pane[zPosition] as ForceFloor).switchImage();
            } else if (pane[zPosition] instanceof Door) {
              (pane[zPosition] as Door).color = (tmpObj as Door).color;
              (pane[zPosition] as Door).switchImage();
            } else if (pane[zPosition] instanceof Key) {
              (pane[zPosition] as Key).color = (tmpObj as Key).color;
              (pane[zPosition] as Key).switchImage();
            } else if (pane[zPosition] instanceof PinkBall) {
              (pane[zPosition] as PinkBall).direction = (tmpObj as PinkBall).direction;
            } else if (pane[zPosition] instanceof Shoe) {
              (pane[zPosition] as Shoe).shoeType = (tmpObj as Shoe).shoeType;
              (pane[zPosition] as Shoe).switchImage();
            } else if (pane[zPosition] instanceof Switchable) {
              (pane[zPosition] as Switchable).switchColor = (tmpObj as Switchable).switchColor;
              (pane[zPosition] as Switchable).currentState = (tmpObj as Switchable).currentState;
              (pane[zPosition] as Switchable).switchImage();
            } else if (pane[zPosition] instanceof ThinWall) {
              (pane[zPosition] as ThinWall).direction = (tmpObj as ThinWall).direction;
              (pane[zPosition] as ThinWall).direction2 = (tmpObj as ThinWall).direction2;
              (pane[zPosition] as ThinWall).switchImage();
            } else if (pane[zPosition] instanceof Tank) {
              (pane[zPosition] as Tank).direction = (tmpObj as Tank).direction;
              (pane[zPosition] as Tank).switchImage();
            } else if (pane[zPosition] instanceof Bug) {
              (pane[zPosition] as Bug).direction = (tmpObj as Bug).direction;
              (pane[zPosition] as Bug).switchImage();
            } else if (pane[zPosition] instanceof Teleporter) {
              (pane[zPosition] as Teleporter).positionConnectedTeleporter = (tmpObj as Teleporter).positionConnectedTeleporter;
            }

            // add the tile in case it is triggered by time
            if ((pane[zPosition] as TimeTrigger).tickDuration !== null && (pane[zPosition] as TimeTrigger).tickDuration >= 1) {
              this.timerService.tiles.push(pane[zPosition] as TimeTrigger);
            }
          }
        }
        column.push(pane);
      }
      this.tiles.push(column);
    }

    this.blockSwitchService.loadLevel(this);

    if (startTimerTiles) {
      this.timerService.startTimer();
    }
    return this;
  }

  public loadLevel(levelName: string = '') {
    // load levels to be played
    if (this.levelList.length === 0) {
      if (this.multiplayerService.isMultiplayer) {
        this.levelList = JsonLevelListMultiplayer;
      } else {
        this.levelList = JsonLevelListSingleplayer;
      }
    }

    // reset old game state
    this.timerService.stopTimer();
    this.inventory.clear();
    this.$battery = 0;
    this.player = null;
    this.secondPlayer = null;

    if (levelName.trim() !== '') {
      this.levelName = levelName;

    } else if (!this.levelName) {
      // load first level when there is no other level loaded
      this.levelName = this.levelList[0];

    } else {
      const index = this.levelList.indexOf(this.levelName);
      if (this.levelList.length < index + 1) {
        // player has won
        alert('You have won!');
        // TODO improve message

      } else {
        this.levelName = this.levelList[index + 1];
      }
    }

    if (this.levelName === 'emptyLevel') {
      // generate emptyLevel
      this.generateEmptyLevel();
    } else {
      this.http.get(`assets/levels/${this.levelName}.json`).subscribe(
        level => {
          this.deserialize(level as LevelHandlerService);
        });
    }
  }

  /**
   * generate an emptyLevel which is used in the LevelEditor
   */
  private generateEmptyLevel(): void {
    this.player = null;

    if (this.tiles) {
      while (this.tiles.length !== 0) {
        this.tiles.pop();
      }
    } else {
      this.tiles = new Array<Array<Array<Tile>>>();
    }

    for (let yPosition = 0; yPosition < this.getSizeOfDimension('y'); yPosition++) {
      this.tiles.push(this.generateEmptyColumn());
    }

  }

  public getLevelGridTiles(): Tile[][][] {
    return this.tiles;
  }

  public getZStack(position: Array<number>): Array<Tile> {
    if (!position || position.length === 0) {
      return null;
    }
    return this.tiles[position[0]][position[1]];
  }

  /**
   * @returns Array [z][y][x]
   */
  public getTilesGroupedPerPane(): Array<Array<Array<Tile>>> {
    let paneArray: Tile[][][] = new Array<Array<Array<Tile>>>(this.getSizeOfDimension('z'));

    // arrays need to be copied and not taken by reference
    // that's the reason for this crazy null filling and mapping afterwards
    paneArray = new Array(this.getSizeOfDimension('z')).fill(null).map(() => (
      new Array(this.getSizeOfDimension('y')).fill(null).map(() => (
        new Array(this.getSizeOfDimension('x'))
      )))
    );

    for (let yCoord = 0; yCoord < this.tiles.length; yCoord++) {
      const row = this.tiles[yCoord];

      for (let xCoord = 0; xCoord < row.length; xCoord++) {
        const stack = row[xCoord];

        for (let zCoord = 0; zCoord < stack.length; zCoord++) {
          let tile = stack[zCoord];

          // ignore non-defined tiles in panes
          if (!tile) {
            tile = null;
          }

          paneArray[zCoord][yCoord][xCoord] = tile;
        }
      }
    }

    return paneArray;
  }

  /**
   * Return the size of the dimension (y=rows, x=colums, z=pane)
   */
  public getSizeOfDimension(dimension: 'y' | 'x' | 'z'): number {
    switch (dimension) {
      case 'y':
        return this.yCoordSize;
      case 'x':
        return this.xCoordSize;
      case 'z':
        return Object.keys(StackLayer).length / 2;
      default:
        throw new TypeError('wrong dimension passed.');
    }
  }

  /**
   * Find position of tile in level.
   * Find position of block in level.
   * @returns number [y][x] !
   */
  public getBlockPosition(tile: Tile): number[] {
    const location = new Array(3);
    // used as an exception to escape multiple for loops and the if, cause its not possible to return in a differnt way
    const BreakException = {};

    try {
      this.tiles.forEach((element, yCoord) => {
        element.forEach((element2, xCoord) => { // TODO: row/column?
          const zCoord = element2.indexOf(tile);
          if (zCoord !== -1) {
            location[0] = yCoord;
            location[1] = xCoord;
            location[2] = zCoord;
            throw BreakException;
          }
        });
      });
    } catch (e) {
      if (e !== BreakException) {
        throw e;
      }
    }

    if (!location || (!location[0] && location[0] !== 0) || (!location[1] && location[1] !== 0) || (!location[2] && location[2] !== 0)) {
      return null;
    }

    return location;
  }

  /**
   * trigger the move for the tile // TODO: or is this actually just checking?
   */
  public moveBlock(tile: Tile, direction: MoveDirection, teleportPosition: Array<number> = null): boolean {
    const currentPosition = this.getBlockPosition(tile);
    if (!currentPosition) {
      return false;
    }
    const currentPositionStack = this.getZStack(currentPosition);
    const newPosition = this.getNewPosition(currentPosition, direction, teleportPosition);

    // in case the new position is outside of the level, prevent mode
    if (newPosition.includes(-1) || currentPosition === newPosition) {
      return false;
    }

    const newPositionStack = this.getZStack(newPosition);

    if (!newPositionStack) {
      return false;
    }

    this.executePreCheckEvent(tile, direction, currentPositionStack);
    this.executePreCheckEvent(tile, direction, newPositionStack);

    if (!this.executeCanMoveFromHere(tile, direction, currentPositionStack)) {
      return false;
    }

    if (!this.executeCanMoveToHere(tile, direction, newPositionStack)) {
      return false;
    }

    this.executePreMoveEvent(tile, direction, currentPositionStack);
    this.executePreMoveEvent(tile, direction, newPositionStack);

    this.executeMoveBlock(tile, direction, teleportPosition);

    this.executePostMoveEvent(tile, direction, currentPositionStack);
    this.executePostMoveEvent(tile, direction, newPositionStack);

    return true;
  }

  /**
   * physically move the tile to the new location
   *
   * @param tile the tile that should be moved
   * @param direction the direction in which it should be moved
   * @param teleportPosition position the tile is teleported to
   */
  private executeMoveBlock(tile: Tile, direction: MoveDirection, teleportPosition: Array<number> = null) {
    const currentPosition = this.getBlockPosition(tile);
    const newPosition = this.getNewPosition(currentPosition, direction, teleportPosition);

    if (
      currentPosition[0] === newPosition[0] &&
      currentPosition[1] === newPosition[1] &&
      currentPosition[2] === newPosition[2]) {
      return;
    }

    this.tiles[newPosition[0]][newPosition[1]][newPosition[2]] = tile;

    const newEmptyBlock = new EmptyPlaceholder(this);
    this.tiles[currentPosition[0]][currentPosition[1]][currentPosition[2]] = newEmptyBlock;
  }

  public executeCanMoveFromHere(blockToMove: Tile, direction: MoveDirection, currentPositionStack: Array<Tile>): boolean {
    let canMoveFromHere = true;

    currentPositionStack.forEach(tile => {
      if (tile !== undefined) {
        if (!tile.canMoveFromHere(blockToMove, direction)) {
          canMoveFromHere = false;
        }
      }
    });

    return canMoveFromHere;
  }

  public executeCanMoveToHere(blockToMove: Tile, direction: MoveDirection, newPositionStack: Array<Tile>): boolean {
    let canMoveToHere = true;

    newPositionStack.forEach(tile => {
      if (tile !== undefined) {
        if (!tile.canMoveToHere(blockToMove, direction)) {
          canMoveToHere = false;
        }
      }
    });

    return canMoveToHere;
  }

  private executePreCheckEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    tileStack.forEach(element => {
      if (element !== undefined) {
        element.preCheckEvent(blockToMove, direction, tileStack);
      }
    });
  }

  private executePreMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    tileStack.forEach(element => {
      if (element !== undefined) {
        element.preMoveEvent(blockToMove, direction, tileStack);
      }
    });
  }

  private executePostMoveEvent(blockToMove: Tile, direction: MoveDirection, tileStack: Array<Tile>) {
    tileStack.forEach(element => {
      if (element !== undefined) {
        element.postMoveEvent(blockToMove, direction, tileStack);
      }
    });
  }

  public getNewPosition(position: number[], direction: MoveDirection, teleportPosition: Array<number> = null): number[] {
    let newPosition = Object.assign([], position);

    if (teleportPosition) {
      newPosition = Object.assign([], teleportPosition);
    } else {
      switch (direction) {
        case MoveDirection.north:
          newPosition[0] += -1;
          break;

        case MoveDirection.south:
          newPosition[0] += 1;
          break;

        case MoveDirection.east:
          newPosition[1] += 1;
          break;

        case MoveDirection.west:
          newPosition[1] += -1;
          break;
      }
    }

    // handle array overflow by not moving anywhere
    if (this.tiles.length <= newPosition[0] || this.tiles[0].length <= newPosition[1]) {
      return position;
    }

    return newPosition;
  }

  public getLastPosition(position: number[], direction: MoveDirection): number[] {
    let newDirection: MoveDirection;
    switch (direction) {
      case MoveDirection.north:
        newDirection = MoveDirection.south;
        break;

      case MoveDirection.south:
        newDirection = MoveDirection.north;
        break;

      case MoveDirection.east:
        newDirection = MoveDirection.west;
        break;

      case MoveDirection.west:
        newDirection = MoveDirection.east;
        break;
    }

    return this.getNewPosition(position, newDirection);
  }

  public getInventoryItems(): Array<Array<Tile>> {
    return this.inventory.getInventoryItems(4);
  }


  public getInventory(): InventoryHandlerService {
    return this.inventory;
  }

  public addToInventory(tile: Collectable): boolean {
    return this.inventory.add(tile);
  }

  public createNewBlockAtPosition(tile: Tile, position: Array<number>) {
    const newBlockInstance = GameBlockFactory.createGameBlock(tile, this);

    if (tile instanceof Player) {
      const emptyBlock = new EmptyPlaceholder(this);
      const emptyBlock2 = new EmptyPlaceholder(this);

      // make sure that the player cannot be stuck in another Tile
      // but it can be on top of a thin wall
      if (!(this.tiles[position[0]][position[1]][StackLayer.block] instanceof ThinWall)) {
        this.tiles[position[0]][position[1]][StackLayer.block] = emptyBlock;
      }

      this.tiles[position[0]][position[1]][StackLayer.moveable] = emptyBlock;

    } else if (tile.getStackZCoord() === StackLayer.block
      || tile.getStackZCoord() === StackLayer.moveable
      && this.getBlockPosition(this.player)
      && this.positionEqual(this.getBlockPosition(this.player), position)) {
      // in case ther is another tile at this Position which is not a EmptyBlock

      const emptyBlock = new EmptyPlaceholder(this);
      this.tiles[position[0]][position[1]][StackLayer.player] = emptyBlock;
      this.player = null;
    }

    if (newBlockInstance instanceof EmptyPlaceholder) {
      // EmptyBlock can be inserted anywhere
      this.tiles[position[0]][position[1]][position[2]] = newBlockInstance;
    } else {
      this.tiles[position[0]][position[1]][newBlockInstance.getStackZCoord()].destroy();
      this.tiles[position[0]][position[1]][newBlockInstance.getStackZCoord()] = newBlockInstance;
    }
  }

  /**
   * remove a Tile and replace it with a EmptyBlock
   *
   * @param tile the tile that should be removed
   */
  public removeBlock(tile: Tile) {
    const position = this.getBlockPosition(tile);
    if (!position) {
      return;
    }
    this.createNewBlockAtPosition(new EmptyPlaceholder(this), position);
  }

  public serializeLevel(): string {
    return JSON.stringify(this, this.replacer);
  }

  public replacer(key: string, value: any) {
    const ignoredProperties = [
      'levelHandler',
      'http',
      'tick',
      'ngUnsubscribe',
      'triggerSizeUpdate$',
      'blockSwitchService',
      'batteryChanged'
    ];

    if (ignoredProperties.includes(key)) {
      return undefined;
    }
    return value;
  }

  /**
   * switch all Blocks which are in the specific color group
   * @param color color of the Group
   * @param switcherBlock switchBlock that triggered the switch (the activator button not the player or monster)
   */
  public SwitchColorGroup(color: SwitchColor, switcherBlock: SwitchTrigger): void {
    this.blockSwitchService.SwitchColorGroup(color, switcherBlock);
  }

  /**
   * helper function to check if the given tile is at the dame Position as the Player (without z coordinate)
   * @param tile Tile to check for x and y coordinates
   */
  public blockHasSamePositionAsPlayer(tile: Tile) {

    const thisPosition = this.getBlockPosition(tile);
    const playerPosition = this.getBlockPosition(this.player);

    if (this.multiplayerService.isMultiplayer) {
      const playerTwoPosition = this.getBlockPosition(this.secondPlayer);
      if ((this.player && this.positionEqual(thisPosition, playerPosition))
        || this.secondPlayer && this.positionEqual(thisPosition, playerTwoPosition)) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.player && this.positionEqual(thisPosition, playerPosition)) {
        return true;
      } else {
        return false;
      }
    }

  }

  /**
   * will be called when the player is killed
   */
  // TODO add some kind of notice to the player that he died
  public killPlayer(sendServer: boolean = true) {
    if (sendServer) {
      this.multiplayerService.sendPlayerChange(PlayerChangeEvent.die);
    }
    this.loadLevel(this.levelName);
  }

  /**
   * gets a tile at the given position
   * @param position position with y,x and z coordinates
   * @param zCoord overrides the zCoord of the position
   * @returns tile at this position
   */
  public getTileAtPosition(position: Array<number>, zCoord: StackLayer = null): Tile {
    if (zCoord) {
      return this.tiles[position[0]][position[1]][zCoord];
    }
    return this.tiles[position[0]][position[1]][position[2]];
  }

  public positionEqual<T>(array1: Array<T>, array2: Array<T>): boolean {
    if (array1.length !== array2.length) {
      return false;
    }
    for (let i = array1.length; i--;) {
      // only check x and y coordinates and not z
      if (i === 2) {
        continue;
      }
      if (array1[i] !== array2[i]) {
        return false;
      }
    }

    return true;
  }

  private generateEmptyPane(): Array<Tile> {
    const pane: Tile[] = new Array<Tile>();
    for (let zPosition = 0; zPosition < this.getSizeOfDimension('z'); zPosition++) {

      if (zPosition === StackLayer.texture) {
        pane.push(GameBlockFactory.createGameBlock(new WoodBackground(this), this));
      } else {
        pane.push(GameBlockFactory.createGameBlock(new EmptyPlaceholder(this), this));
      }
    }
    return pane;
  }

  public generateEmptyColumn() {
    const column: Tile[][] = new Array<Array<Tile>>();
    for (let xPosition = 0; xPosition < this.getSizeOfDimension('x'); xPosition++) {
      column.push(this.generateEmptyPane());
    }
    return column;
  }

  /**
   * resize the levelGrid
   * @param direction in which direction a column/ row should be added/ deleted (south would be at the bottom etc.)
   * @param count how many columns/ rows should be added (positive number) or deleted (negative number)
   */
  public resizeLevelGrid(direction: MoveDirection, count: number) {
    const removedColumn = new Array<Array<Array<Tile>>>();
    const removedRow = new Array<Array<Tile>>();

    switch (direction) {
      case MoveDirection.north:
        if (count > 0) {
          for (let yPosition = 0; yPosition < count; yPosition++) {
            this.tiles.unshift(this.generateEmptyColumn());
          }
          this.yCoordSize++;
        } else if (count < 0) {
          for (let yPosition = 0; yPosition > count; yPosition--) {
            removedColumn.push(this.tiles.shift());
          }
          this.yCoordSize--;
        }
        break;

      case MoveDirection.south:
        if (count > 0) {
          for (let yPosition = 0; yPosition < count; yPosition++) {
            this.tiles.push(this.generateEmptyColumn());
          }
          this.yCoordSize++;
        } else if (count < 0) {
          for (let index = 0; index > count; index--) {
            removedColumn.push(this.tiles.pop());
          }
          this.yCoordSize--;
        }
        break;

      case MoveDirection.east:
        if (count > 0) {
          this.tiles.forEach(row => {
            for (let yPosition = 0; yPosition < count; yPosition++) {
              row.push(this.generateEmptyPane());
            }
          });
          this.xCoordSize++;
        } else if (count < 0) {
          this.tiles.forEach(row => {
            for (let yPosition = 0; yPosition > count; yPosition--) {
              removedRow.push(row.pop());
            }
          });
          this.xCoordSize--;
        }
        break;

      case MoveDirection.west:
        if (count > 0) {
          this.tiles.forEach(row => {
            for (let xPosition = 0; xPosition < count; xPosition++) {
              row.unshift(this.generateEmptyPane());
            }
          });
          this.xCoordSize++;
        } else if (count < 0) {
          this.tiles.forEach(row => {
            for (let xPosition = 0; xPosition > count; xPosition--) {
              removedRow.push(row.shift());
            }
          });
          this.xCoordSize--;
        }
        break;
    }

    removedColumn.forEach(row => {
      row.forEach(tileStack => {
        tileStack.forEach(tile => {
          tile.destroy();
        });
      });
    });

    removedRow.forEach(tileStack => {
      tileStack.forEach(tile => {
        tile.destroy();
      });
    });
  }

  private handlePlayerChangeByEvent(player: Player, playerChangeEvent: PlayerChangeEvent) {
    console.log('handle Player Change By Event');

    switch (playerChangeEvent) {
      case PlayerChangeEvent.moveDown:
        this.moveBlock(player, MoveDirection.south);
        break;

      case PlayerChangeEvent.moveLeft:
        this.moveBlock(player, MoveDirection.west);
        break;

      case PlayerChangeEvent.moveUp:
        this.moveBlock(player, MoveDirection.north);
        break;

      case PlayerChangeEvent.moveRight:
        this.moveBlock(player, MoveDirection.east);
        break;

      case PlayerChangeEvent.die:
        this.killPlayer(player === this.player);
        break;
    }
  }

  private handlePlayerChangeByDirection(player: Player, direction: MoveDirection) {
    console.log('handle Player Change By Direction');

    switch (direction) {
      case MoveDirection.south:
        this.moveBlock(player, MoveDirection.south);
        break;

      case MoveDirection.west:
        this.moveBlock(player, MoveDirection.west);
        break;

      case MoveDirection.north:
        this.moveBlock(player, MoveDirection.north);
        break;

      case MoveDirection.east:
        this.moveBlock(player, MoveDirection.east);
        break;
    }
  }
}
