export enum PlayerChangeEvent {
  moveUp = 'moveUp',
  moveRight = 'moveRight',
  moveDown = 'moveDown',
  moveLeft = 'moveLeft',
  die = 'die',
}
