import { TestBed } from '@angular/core/testing';

import { MultiplayerService } from './multiplayer.service';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';

describe('MultiplayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [SocketIoModule.forRoot({ url: 'http://localhost:4444', options: {} })] // mock NOOP socket service
  }));

  it('should be created', () => {
    const service: MultiplayerService = TestBed.get(MultiplayerService);
    expect(service).toBeTruthy();
  });
});
