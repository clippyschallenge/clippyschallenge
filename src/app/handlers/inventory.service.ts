import { ShoeType } from './../models/shoeType';
import { Injectable } from '@angular/core';
import { EmptyPlaceholder } from '../models/tiles/empty-placeholder';
import { Collectable } from '../models/tiles/abstract/collectable';
import { ColorItem } from '../models/tiles/abstract/colorItem';
import { Shoe } from '../models/tiles/shoe';

@Injectable({
  providedIn: 'root'
})
export class InventoryHandlerService {

  private inventory: Array<Collectable>;

  /**
   * total size of the inventory
   */
  private inventorySize = 8;

  constructor() {
    this.clear();
  }

  /**
   * get a formatted list of inventory items
   * @param elementsPerArray how many elements should be in the inner Array
   * @returns twodimensional list of inventory items
   */
  public getInventoryItems(elementsPerArray: number): Array<Array<Collectable>> {
    // TODO: should just return a single list, formatting in two rows should be done by CSS, automatically
    const dimensionalInventory = new Array<Array<Collectable>>(0);

    for (let index = 0; index < (this.inventorySize / elementsPerArray); index++) {
      dimensionalInventory[index] = new Array<Collectable>(0);

      const elements = this.inventory.slice(index * elementsPerArray, index * elementsPerArray + elementsPerArray);
      dimensionalInventory[index] = elements;
    }

    return dimensionalInventory;
  }

  /**
   * try to add a tile in the inventory
   * @param item Tile that should be added in the inventory
   * @returns true if it could be added and false if the space is not sufficient
   */
  public add(item: Collectable): boolean {

    for (let index = 0; index < this.inventory.length; index++) {
      // if slot is not empty, try next slot
      if (!(this.inventory[index] instanceof EmptyPlaceholder)) {
        continue;
      }

      this.inventory[index] = item;
      return true;
    }
    return false;
  }

  /**
   * check if ther is an item in the inventory of the player
   * @param itemType instanceof Collectable for a normalItem ot colorItem, ShoeType for a shoeItem
   * @param type specify the given itemType
   */
  public hasItemInInventory(itemType: any, type: 'normalItem' | 'colorItem' | 'shoeItem'): boolean {
    let inInventory = false;

    // TODO: refactor? why not just use `filter`?
    // used as an exception to escape multiple for loops and the if, cause its not possible to return in a differnt way
    const BreakException = {};
    try {
      this.inventory.forEach(element => {
        if (itemType instanceof Collectable && type === 'normalItem' && element['@type'] === itemType['@type']) {
          inInventory = true;
          throw BreakException;

        } else if (itemType instanceof Collectable &&
          type === 'colorItem' &&
          element['@type'] === itemType['@type'] &&
          element.$color === itemType.$color
        ) {
          inInventory = true;
          throw BreakException;

        } else if (type === 'shoeItem' && element instanceof Shoe && element.$shoeType === itemType as ShoeType) {
          inInventory = true;
          throw BreakException;
        }
      });
    } catch (e) {
      if (e !== BreakException) {
        throw e;
      }
    }
    return inInventory;
  }

  /**
   * clear the whole inventory
   */
  public clear(): void {
    this.inventory = new Array<Collectable>(0);

    // fill with empty tiles
    for (let inventoryItem = 0; inventoryItem < this.inventorySize; inventoryItem++) {
      // null is only temporarily and will be exchanged when a real item is added to the inventory
      this.inventory.push(new EmptyPlaceholder(null));
    }
  }
}
