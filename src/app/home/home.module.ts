import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { LevelStatusSidebarComponent } from '../level/level-status-sidebar/level-status-sidebar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GameEngineSharedModule } from '../shared/game-engine.shared.module';
import { ApiInterceptor } from '../helper/apiInterceptor';

@NgModule({
  declarations: [
    HomePage,
    LevelStatusSidebarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    GameEngineSharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
  ],
})
export class HomePageModule { }
