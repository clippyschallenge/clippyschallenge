import { MoveDirection } from './../models/move-direction';
import { LevelHandlerService } from '../handlers/level.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HostListener } from '@angular/core';
import { MultiplayerService } from '../handlers/multiplayer.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  constructor(private levelHandlerService: LevelHandlerService, private multiplayerService: MultiplayerService) {
  }

  isMultiplayer = false;
  latency: number;
  e2eLatency: number;
  room: string = this.multiplayerService.currentRoom;

  private pongSubscription$;
  private e2ePongSubscription$;

  ngOnInit() {
    this.isMultiplayer = this.multiplayerService.isMultiplayer;
    this.pongSubscription$ = this.multiplayerService.pongLatency$.subscribe((latency) => this.latency = latency);
    this.e2ePongSubscription$ = this.multiplayerService.e2eLatency$.subscribe((e2eLatency) => this.e2eLatency = e2eLatency);

    this.levelHandlerService.loadLevel();
  }

  ngOnDestroy() {
    this.pongSubscription$.unsubscribe();
    this.e2ePongSubscription$.unsubscribe();
  }

  async levelTimedOut() {
    // const modal = await this.modalController.create({
    //   component: HomePage // TODO add page for resetting the level and add this as a function
    // });
    // return await modal.present();
  }

  @HostListener('document:keydown', ['$event'])
  keyListenerEvent(event: KeyboardEvent) {
    if (event.key === 'ArrowUp' || event.key === 'w') {
      this.levelHandlerService.timerService.nextPlayerMove = MoveDirection.north;
    } else if (event.key === 'ArrowDown' || event.key === 's') {
      this.levelHandlerService.timerService.nextPlayerMove = MoveDirection.south;
    } else if (event.key === 'ArrowLeft' || event.key === 'a') {
      this.levelHandlerService.timerService.nextPlayerMove = MoveDirection.west;
    } else if (event.key === 'ArrowRight' || event.key === 'd') {
      this.levelHandlerService.timerService.nextPlayerMove = MoveDirection.east;
    }
  }
}
