import { Component, OnInit, OnDestroy } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MultiplayerService } from './handlers/multiplayer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private multiplayerService: MultiplayerService,
    private alertController: AlertController
  ) {
    this.initializeApp();
  }

  private errorMessageSubscription: Subscription;

  ngOnInit() {
    this.errorMessageSubscription = this.multiplayerService.errorMessage$.subscribe(
      (message) => this.presentAlert('Critical error', message)
    );
  }

  ngOnDestroy() {
    this.errorMessageSubscription.unsubscribe();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  private async presentAlert(header: string, message: string) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: ['OK']
    });

    await alert.present();
  }
}
