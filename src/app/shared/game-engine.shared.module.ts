import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LevelGridComponent } from '../level/level-grid/level-grid.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { LevelTileComponent } from '../level/level-tile/level-tile.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    LevelGridComponent,
    SidebarComponent,
    LevelTileComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [
    CommonModule,
    LevelGridComponent,
    SidebarComponent,
    LevelTileComponent,
  ]
})
export class GameEngineSharedModule { }
