# Contributing

Nice to see you want to contribute! :+1: :tada:
Please have a look at this guide to know what and how to best change things, get tips on how to get started and (best) practices to follow.

## Building

This project was generated with [Ionic CLI](https://ionicframework.com/docs/installation/cli) version 5.4.2.

### Install dependencies

1. Clone the source code.
2. Install npm, if needed.
3. Then install the dependencies for npm:
   ```shell
   $ npm install
   ```

### Development server

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100`. The app will automatically reload if you change any of the source files.

For live debugging add a config like in this example to VSCode and run it.
```json
{
  "name": "Serve to the browser (ionic serve)",
  "type": "cordova",
  "request": "launch",
  "platform": "serve",
  "devServerAddress": "localhost",
  "sourceMaps": true,
  "ionicLiveReload": true,
  "cwd": "${workspaceFolder}"
}
```

Run `ionic cordova emulate android` for emulating the app.

### App

To generate an App out of the Files run `ionic cordova build <platform> [options]`. use the `--debug` for a debug and `--prod` flag for a production build. For a list of all Commands see `https://ionicframework.com/docs/cli/commands/cordova-build`.

### Code scaffolding

Run `ionic generate` or `ionic generate component component-name` to generate a new component. You can also use `ng generate page|component|service|module|class|directive|guard|pipe|interface|enum`.

### Build

Run `ionic build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Linting

Run `npm run lint` to lint the code.

This includes:
* the [Angular linting](https://angular.io/cli/lint) (i.e. TypeScript linting via tslint)
* stylelint linting

### Running unit tests

Run `npm run test:dev` to execute the unit tests via [Karma](https://karma-runner.github.io).

Note that `npm run test` is intended for the CI build.
Run `npm run test:devFirefox` for running the tests via Firefox.

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Ionic CLI `ionic help` or go check out the [Ionic CLI README](https://github.com/ionic-team/ionic-cli/blob/develop/README.md).

## Coding

As for simple indentation issues, please refer to the _editorconfig file_. Just use a [plugin](https://editorconfig.org/#download), if needed, for your editor.

Apart from that, there are some simple rules.

### JS
* Use EcmaScript 2019. (so e.g. `await`/`async` are fine) Basically everything, which is supported by modern browsers. (It will be transpiled anyway.)
* We use [TSLint](https://palantir.github.io/tslint/). Please do use it to lint your files. It specifies many coding guidelines.
* We use [ESLint](https://eslint.org/). Please do use it to lint your files. It specifies many coding guidelines.
  When something is not specified just use common sense and look at how other code in the project is written.
* Use early return instead of nested if blocks to keep the code readable.
* Use `const` whenever possible (also in local variables in functions), only use `let` when the variable needs to be changed. Don't use `var`.
* Do _not_ use magic numbers. Use (global/module-scoped) constants instead.
* Use logging via `console.log`, `console.warn` etc. often.

### TypeScript

* Do use TypeScripts short syntax for saving [dependency-injected services](https://angular.io/guide/dependency-injection-providers#class-providers-with-dependencies) in methods by specifying them in the constructor.
* Only inject depdencies in the constructor, do everything else to initialize a component in `ngOnInit`. There [is not much difference](https://stackoverflow.com/questions/35763730/difference-between-constructor-and-ngoninit). but it results in cleaner code.
* Always use private functions first, only if needed make them public.
  Note that in TypeScript all functions [are public by default](https://stackoverflow.com/questions/37506343/private-and-public-in-angular-component#37506946).

### Angular/RxJS 

* Prefer RxJS over native JavaScript functions, if possible.
* Unsubscribe from all subscribed events.
  Please do use the pattern of having a private `ngUnsubscribe`, where you [`takeUntil`](https://rxjs.dev/api/operators/takeUntil) all observables to. See [this Stackoverflow answer](https://stackoverflow.com/a/41177163) at the bottom.
* If you save Observables in variables _end them_ with a dollar sign (`$`), e.g. `exampleData$` to differentiate them from the actual data object.
* Use constructors only for dependency injection. Otherwise use [](https://angular.io/api/core/OnInit)
* One empty line before and one empty line after the decorators (e.g. `@Component`).

### (S)CSS

* Please do use [stylelint](https://stylelint.io/). A config file is included in the project.

## Game internals

For naming variables, files and writing comments etc., please be as specific as possible.

Look at the glossary/terminology wiki page for how things should be named:

https://gitlab.com/clippyschallenge/clippyschallenge/wikis/terminology
 